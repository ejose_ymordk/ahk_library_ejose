#include ymorlib\Ymorlib.ahk ; include the ymor lib so we can use the ymor functions.
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SetTitleMatchMode, 2 ; A window's title can contain WinTitle anywhere inside it to be a match.
CoordMode, Mouse, Relative ; Coordinates are relative to the active window.
global yCreateDocumentation := false ; boolean that is used to create the Ymor script documentation i.e. Designdocument
global totalscripttime, sUsername, sPassword ; , params ; Declare global variables for use in all transactions. totalscripttime is for total script time. sUsername and sPassword are used in paramsCSV file. 

main() ; Start function main, which basically starts and runs the script we define below.

main() ; Main function
{
	measurement := init() ; First run the init
	try ; Try catch so if a transaction fails the script will stop but will still finish the transaction itself.
	{
		hello(measurement) ; We use number so it is easier to sort when using admin or creating dashboards when script has been uploaded
		world(measurement) ; We use number so it is easier to sort when using admin or creating dashboards when script has been uploaded
		trans(measurement) ; We use number so it is easier to sort when using admin or creating dashboards when script has been uploaded
	} catch e {
		Debug.logMessage("ERROR", "Exception in: " e.File ", linenumber: " e.Line ", errormessage: " e.Message ", function: " e.What ) ; Log to logger in Scite.
	}
	finalise(measurement) ; Stop the measurement and parse the results.
}

init() {
	Debug.setLevel("SEVERE") ; Since this is the test template script we will set the debug level to log everything. Allowed levels are; FINE, CONFIG, INFO, WARNING, SEVERE. Try them all. :)
	FileFunctions.ClearLogFolder() ; Clear the logfiles in the output2 folder. This can be useful when making a script and you want a new logfile for each run.s
	measurement := new Measurement("YDummyScriptName",900) ; Start a new measurement with a timeout of 900 seconds which is 15 minutes. This is the default value. Input you set is seconds.
	measurement.setImageDir("\Images\") ; Set the imagedir. Currently not adjustable.
    ;~ transaction.yKillAllExcept([""],[""],true) ; Command that will kill all open programs. This is a Testrun, which will show message boxes of all open programs and what will happen to each program (kill or no kill).
	;~ params := FileFunctions.ReadCSV("params.csv", A_ComputerName) ; Read sentinel dependent parameters from a csv file and place in variable.
	;~ sUsername := params["sUsername"] ; Get username from the file and save to variable for later use.
    ;~ sPassword := params["sPassword"] ; Get password from the file and save to variable for later use.
	return measurement
}

hello(measurement)
{
	trans := measurement.newTransaction("hello") ; Declare a new transaction within the measurement.
	trans.start() ; When starting a transaction the timer will also be started automatically.
	; insert transaction steps
	trans.stop() ; Stop the timer and the transaction.
}

world(measurement)
{
	trans := measurement.newTransaction("world") ; Declare a new transaction within the measurement.
	trans.start() ; When starting a transaction the timer will also be started automatically.
	; insert transaction steps
	trans.stop() ; Stop the timer and the transaction.
}

trans(measurement)
{
	trans := measurement.newTransaction("trans") ; Declare a new transaction within the measurement.
	trans.start() ; When starting a transaction the timer will also be started automatically.
	; insert transaction steps
	trans.stop() ; Stop the timer and the transaction.
}

finalise(measurement) {
	measurement.stop() ; Stop the measurement
	measurement.parseResult("LOG") ; Parse the results to the logfile and result file
}

;------------------------------------------ DECLARE TRANSACTIONS FOR UPLOADING ------------------------------------------;
; This is necessary so that the transactions are being detected when uploading in the admin.
trans := srun.start_transaction("hello") ; Points in name are not Allowed.
trans := srun.start_transaction("world") ; Points in name are not Allowed.
trans := srun.start_transaction("trans") ; Points in name are not Allowed.
