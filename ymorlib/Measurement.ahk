﻿; Include the lib files this class needs.  
#include ymorlib\Debug.ahk
#include ymorlib\Transaction.ahk
#include ymorlib\ScriptResult.ahk
#include ymorlib\LogParser.ahk
#include ymorlib\FileFunctions.ahk
#include ymorlib\Mail.ahk
#include ymorlib\Notification.ahk

#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
Global YStartTijd ; Define global for use in all functions.

; @doc When default timeout (15 minutes) has been reached for the measurement run, kill the scriptrun.
; @doc See Constructor for default timeout.
ForceExitApp() {
	SetTimer,  ForceExitApp, Off
	Debug.logMessage("SEVERE", "Script has run longer than allowed time, killing script.")
	Measurement.Halt() 
}

/**
* Main class that controls the measurement runs. 
*
*/

class Measurement {
	
	; @doc Declare variables.
	starttime := A_TickCount
	timeout := 0 
	result := ""
	scriptResult := ""
	fileLocation := A_ScriptDir . "\output2"
	imageDir := "\"
	transactions := Object()
	transactionsResults := Object()

	; @doc Build constructor.
	; @doc Set timeout for each script to 900 seconds == 15 minutes. 
  __New(tScriptnaam="Default", tTimeout=900) {
		global totalscripttime
		global YStartTijd := A_TickCount
		
		; If output2 directory does not exist then create it.
		if !FileExist(this.fileLocation) {
				fileLocation := this.fileLocation
				FileCreateDir, %fileLocation% 
				if (ErrorLevel) {
					MsgBox, Could not create output2 directory due to limited user rights on OS level, the script will now stop. 
					throw Exception("Could not create output2 directory due to limited user rights on OS level, finishing...", -1)
				}
		}

		;Timeout * 1000 because of milliseconds. 
		Timeout := tTimeout * 1000
		; Pass timeout to ForceExitApp function which then will call the Halt function. 
		SetTimer , ForceExitApp , %Timeout%
		this.setTimeout(tTimeout)
		totalscripttime := 0
		
		this.scriptResult := New ScriptResult()
		Debug.logMessage("TRANS", "-------------------------------------------------------------------- START OF MEASUREMENT ---------------------------------------------------------------------")
    }
	
	; @doc Set the timeout of the measurement.
	; @param tTimeout, the timeout that is set in the user script.
	setTimeout(tTimeout) {
		this.timeout := tTimeout
		this.finaltimeout := this.starttime + (1000 * this.timeout)
	}

	; @doc Check if timeout of measurement has been reached, if so stop the measurement. 
	checkTime() {
		if (A_TickCount > this.finaltimeout) {
			this.transactionsResults := false
			Debug.logMessage("SEVERE", "Script has run longer than allowed time, finishing...")
			Debug.logMessage("INFO", "Tick: " . A_TickCount . ", final: " . this.finaltimeout . ", started: " . this.starttime)
			this.Halt() 
			throw Exception("Script has run longer than allowed time, finishing...", -1)
		}
	}
	
	; @doc Stop measurement will check each transaction for their result, after getting the results it will be added to the scriptresult.
	Stop(){
		hasError := false
		errTX := []
		Debug.logMessage("FINE", "Measurement stopped, saving scriptresult")

		Loop % this.transactions.MaxIndex() {
			transResults := this.transactions[A_Index].getTransactionResults()

			Debug.logMessage("FINE", "Checking: " . this.transactions[A_Index].getName())	
			if (NumGet(&transResults + 4*A_PtrSize) = 0) {
				transResults[1] := New TransactionResult(TN)
				transResults[1].Insert(this.transactionResult)
			}

			; This if statement is used to detect open transactions and closes them with an error status.
			if (transResults[1].errorResult = "") {  
				
				transResults[1].errorResult := new ErrorResult(TN)
				transResults[1].errorResult.setResultStatus("0")
				
				transResults[1].seterrorResult.setMessage("bla")
				transResults[1].setDuration(0.31337)
				Debug.logMessage("FINE", "close open tx")	
			}
			
			this.transactionsResults.Insert(transResults)
			Debug.logMessage("FINE", "Transactionresult added to array")	
			
			if(this.transactions[A_Index].errorResult.getResultStatus() == "0") {
				hasError := true
				errTX.Push(this.transactions[A_Index].getName())
			}
		}
		this.scriptResult.setTransactionResults(this.transactionsResults)
		Debug.logMessage("TRANS", "-------------------------------------------------------------------- END OF MEASUREMENT -----------------------------------------------------------------------")
		
		if(hasError) {
			Mail.sendLog("Error: " . A_ComputerName, errTX)
		}
		Notification.hide()
	}
	
	; @doc Terminate the script unconditionally. 
	Halt(){
		Debug.logMessage("SEVERE", "System halt called, stopping all script actions and terminating the script.")
		Mail.sendLog("Error: " . A_ComputerName, errTX)
		ExitApp
	}
	
	; @doc Inventory of all the transaction within the measurement. 
	; @param transName, Name of each declared transaction within the measurement.
	NewTransaction(transName) {
		mytrans := new Transaction(transname)
		this.transactions.Insert(mytrans)
		mytrans.setImageDir(this.imageDir)
		return mytrans
	}
	
	; @doc Parse the results based on the parser that is used within the measurement.
	; @doc Other parse options will be implemented in a later stage. 
	; @param parsertype, Parser that is declared in the measurement.
	; @param fileLocation, The filelocation where the logfiles are set.
	ParseResult(parserType, fileLocation = "\output2"){
		this.fileLocation := fileLocation
		if(fileLocation == "\output2") {
			Debug.logMessage("INFO", "Default filelocation used , " . A_ScriptDir . "\output2")
			this.fileLocation := A_ScriptDir . "\output2"
		}
		
		if(parserType == "LOG") {
			this.parser := new LogParser(this.scriptResult, this.fileLocation)
			this.parser.parse()
		}
		else if(parserType == "DYNATRACE") {
		}
		else if (parserType == "CSV") {
		}
		else if (parserType == "JSON") {
		}
		else {
			Debug.logMessage("INFO", "No or wrong parser selected, will use default LOG parser.")
			this.ParseResult("LOG")
		}
	}
	
	; @doc Setter for the imagedirectory.
	setImageDir(imageDir) {
		this.imageDir := imageDir
	}
	
	; @doc Getter for the imagedirectory.
	getImageDir(imageDir) {
		return this.imageDir
	}

  }
  

	