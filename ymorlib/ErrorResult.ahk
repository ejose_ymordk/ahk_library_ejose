﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

/**
 * Class that is used to handle the errorresults of a measurement.
 *
 */

class ErrorResult {
	
	name := ""
	; @doc resultStatus can be 0 for failed or 1 for success.
	resultStatus := ""	
	message := ""

	; Build Constructor
	__New(name) {
    this.name := name
	}
	
	; @doc Setter for the resultstatus
	setResultStatus(resultStatus){
		this.resultStatus := resultStatus
	}
	
	; @doc Getter for the resultstatus
	getResultStatus(){
		return this.resultStatus
	}
	
	; @doc Setter for the message
	setMessage(message){
		this.message := message
	}

	; @doc Getter for the message
	getMessage(message){
		return this.message
	}

	; @doc Setter for the creationdate
	setCreationDate(creationDate) { 
		this.creationDate := creationDate
	}

	; @doc getter for the creationdate
	getCreationDate(creationDate) {
		return this.creationDate
	}

	; @doc Setter for the name
	setName(name) {
		this.name := name
	}

	; @doc Getter for the name
	getName(name) {
		return this.Name
	}

	; @doc Setter for the duration
	setDuration(duration) {
		this.duration := duration 
	}

	; @doc Getter for the duration
	getDuration(duration) { 
		return this.duration
	}

	; @doc Setter for the startdate
	setStartDate(startDate) {
		this.startDate := startDate 
	}
	
	; @doc Getter for the startdate
	getStartDate(startDate) {
		return this.startDate
	}

	; @doc Setter for the resultstatus
	setEndDate(endDate) {
		this.endDate := endDate
	}

	; @doc Setter for the resultstatus
	getEndDate(endDate) {
		return this.endDate
	}

	; @doc Setter for the resultstatus
	setMetaDataResults(metaDataResults) { 
		this.metaDataResults := metaDataResults
	}

	; @doc Setter for the resultstatus
	getMetaDataResults(metaDataResults) {
		return this.metaDataResults
	}

	; @doc Setter for the resultstatus
	setErrorResults(errorResults) {
		this.errorResults := errorResults
	}

	; @doc Setter for the resultstatus
	getErrorResults(errorResults) {
		return this.errorResults
	}
	
}