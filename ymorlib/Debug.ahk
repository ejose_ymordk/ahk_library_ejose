﻿#Include ymorlib\Measurement.ahk
#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
;#Warn ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

/**
 * Class that is used to set different Loglevels. 
 * 
 * SEVERE logs ERROR, SEVERE and TRANS.
 * WARNING logs WARNING, ERROR, SEVERE and TRANS.
 * INFO logs INFO, WARNING, ERROR, SEVERE and TRANS.
 * CONFIG logs CONFIG, INFO, WARNING, ERROR, SEVERE and TRANS.
 * FINE logs FINE, CONFIG, INFO, WARNING, ERROR, SEVERE and TRANS.
 */
 
class Debug {

	; We will create a static object of Debug, so every class can use this object to send debug information.
	static Key := Debug.setup()
	level := ""
		
	; @doc Create a static object 
	setup() {
		; Create global debug object.
		global Debug := new Debug()
		; Set default log level to SEVERE.
		Debug.setLevel("SEVERE")
		; Create a filelocation, default filelocation is used.
		this.fileLocation := A_ScriptDir . "\output2"
		Debug.setFileLocation()
	}
	
  __New() {
		; Just leave it empty, there are no actions neccesary.
	}
	
	; @doc set the debuglevel
	setLevel(debugMode) {
		this.debugMode := debugMode		
		this.level := this.getLevel(debugMode)	
		this.file := FileOpen(this.fileName, "w")
				
		if (this.level = "FINE" or this.level = "INFO" or this.level = "SEVERE") {
			; just continue
		}
		; Incorrect loglevel has been set. 
		else {
			FormatTime, time, %A_Now%, yyyy-MM-dd HH:mm:ss  
			this.file.WriteLine("The used debuglevel in your script does not exist. The debuglevel you used is: " . this.level . ". Use one of the following options: FINE, INFO, or SEVERE. The script will now exit.")
			this.file.Close()
			ExitApp
		}
	}
	
	; @ doc get the debuglevel
	getLevel(debugMode) { 
		return this.debugMode
	}
	
	; @doc get the log file name
	getFileName() {
		return this.fileName
	}
	
	; @doc set the filelocation of the logfile
	; @param fileLocation, the location of the logfile
	setFileLocation(fileLocation="DefaultLocation") {
		
		global 0, 1, 2
		val1 = %1%
		val2 = %2%
		
		if (fileLocation == "DefaultLocation"){
			if ("-logfile" = val1) {
				filePath := val2
				this.fileName := filePath  ; this.fileName represends the complete path to the file (used to create file later on)
			}
			else {
				filePath := this.fileLocation . "\logfile.txt"
				this.fileName := filePath ; this.fileName represends the complete path to the file (used to create file later on)
			}
		}
		else if (fileLocation == "") {
			filePath := this.fileLocation . "\logfile.txt"
			this.fileName := filePath ; this.fileName represends the complete path to the file (used to create file later on)
		}
		
		else {
			this.fileLocation := fileLocation
			; If the directory doesn't exist, create it. 
			if !FileExist(fileLocation) { 
				FileCreateDir, %fileLocation% 
				; If we can't create the directory try to create the logfile in the default directory.
				; To-Do -> User does not see when it goes wrong, we need to document this so user know that if logfile is not created at specified location it will try the default location.
				if (ErrorLevel) {
					fileLocation := A_ScriptDir . "\output2" 
					if !FileExist(fileLocation) { 
						FileCreateDir, %fileLocation% 
					}
					this.fileName := fileLocation . "\logfile.txt"
					Debug.logMessage("SEVERE", "Could not create logfile at:" . fileLocation . ". Created logfile in output2 map in scriptdir") 
				}
			}
			this.fileName := fileLocation . "\logfile.txt"
		}
	}

	; @doc function which determines what to write to log
	; @param level, the loglevel
	; @param message, the message that is written to logfile.txt 
	logMessage(level, message) {
		
		; Legacy support for Ymor API, (TRANS without capitals) / error added as loglevel
		if (level == "TRANS") {
			level := "trans"
		}
		else if (level == "ERROR") {
			level := "error"
		}	
			
		if(!this.fileName) {
			if !FileExist(this.fileLocation) {
				fileLocation := this.fileLocation
				FileCreateDir, %fileLocation% 
			}
			; Set this line for use with Jenkins and correct working with parser. This will ensure that the logfile will be written to the workspace when using Jenkins to start scripts!!
			this.fileName := this.fileLocation . "\logfile.txt"
		}
		; SEVERE logs ERROR, SEVERE and TRANS.
		this.file := FileOpen(this.fileName, "a `n")
		
		if (this.debugMode = "SEVERE" and (level = "ERROR" or level = "SEVERE" or level = "TRANS") ){
			FormatTime, time, %A_Now%, yyyy-MM-dd HH:mm:ss
			this.file.WriteLine("[" time "." A_Msec "] [" level "] " message)
		}
		; INFO logs INFO, WARNING, ERROR, SEVERE and TRANS.
		else if(this.debugMode = "INFO" and (level = "INFO" or level = "ERROR" or level = "SEVERE" or level = "TRANS") ){
			FormatTime, time, %A_Now%, yyyy-MM-dd HH:mm:s
			this.file.WriteLine("[" time "." A_Msec "] [" level "] " message)
		}
		; FINE logs FINE, CONFIG, INFO, WARNING, ERROR, SEVERE and TRANS.
		else if(this.debugMode = "FINE" and (level = "FINE" or level = "INFO" or level = "ERROR" or level = "SEVERE" or level = "TRANS") ){
			FormatTime, time, %A_Now%, yyyy-MM-dd HH:mm:ss  
			this.file.WriteLine("[" time "." A_Msec "] [" level "] " message)
		}
		else {
			; Drop the message, because the debuglevel is lower than the message to be safed.
		}
		this.file.Close()
    }

}