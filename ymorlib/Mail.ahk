﻿
#include ymorlib\Debug.ahk

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


class Mail { 
	static Key := Mail.setup()
	
	emailFromAddress := "sentinel.ymor@gmail.com"
	emailPass := "Ym0n1t0rSent1nel"
	
	setup() {
		; Create global debug object.
		global Mail := new Mail()
		; Create a filelocation, default filelocation is used.
		this.fileLocation := A_ScriptDir . "\output2"
	}
	__New() {
		
	}	
	
	setMailTo(to) {
		this.emailToAddress := to
	}
	
	sendLog(emailSubject, errTX)
	{
		global
		IfNotExist, %A_MyDocuments%\mailsend1.17b14.exe
		{
			URLDownloadToFile, https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/mailsend/mailsend1.17b14.exe, %A_MyDocuments%\mailsend1.17b14.exe
		}

		if (StrLen(this.emailToAddress) = 0)
		{
			return false
		}
		
		Debug.logMessage("INFO", "DebugLog: " . Debug.getFileName())
		
		Debug.logMessage("INFO", "Sending mail to: " . this.emailToAddress)
		attachments := ""
		emailSubject .= ": "
		for index, element in errTX ; Enumeration is the recommended approach in most cases.
		{
			Debug.logMessage("INFO", "Failed; " . element)
			img := this.fileLocation . "\" . element . ".png"
			Debug.logMessage("INFO", "img; " . img)
			
			if(FileExist( img ) )
			{
				attachments .= " -attach """ . img . """"
			}
			emailSubject .= element . " "
		}
		
		Debug.logMessage("INFO", "img; " . attachments)
		log_file := Debug.getFileName()
		
		Debug.logMessage("INFO", "log_file; " . log_file)
		
		emailFromAddress := this.emailFromAddress
		emailPass := this.emailPass
		emailToAddress := this.emailToAddress
		emailFromAddress := this.emailFromAddress
		
		;~ RunWait %A_MyDocuments%\mailsend1.17b14.exe -to %emailToAddress% -from %emailFromAddress% -ssl -smtp smtp.gmail.com -port 465 -sub "%emailSubject%" -M "Error" -attach "%log_file%,text/plain,i" +cc +bc -q -auth-plain -user "%emailFromAddress%" -pass "%emailPass%" %attachments% 
		cmd = %A_MyDocuments%\mailsend1.17b14.exe -to %emailToAddress% -from %emailFromAddress% -ssl -smtp smtp.gmail.com -port 465 -sub "%emailSubject%" -M "%emailSubject%" -attach "%log_file%,text/plain,i" +cc +bc -q -auth-plain -user "%emailFromAddress%" -pass "%emailPass%" %attachments% 
		
		;~ Clipboard = %cmd%
		RunWait %cmd%
		
		;~ DetectHiddenWindows On
		;~ Run %ComSpec%,, Hide, pid
		;~ WinWait ahk_pid %pid%
		;~ DllCall("AttachConsole", "UInt", pid)
		;~ WshShell := ComObjCreate("Wscript.Shell")
		;~ exec := WshShell.Exec(cmd)
		;~ output := exec.Stderr.ReadAll()
		;~ MsgBox %output%
		;~ DllCall("FreeConsole")
		;~ Process Close, %pid%
	}

}