﻿#include ymorlib\Debug.ahk ; Include the lib files this class needs.
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

class ScriptResult {
	; Declare variable
	transactionResultArray := object()
	; build constructor
  __New() {
		this.transactionResultArray := 0
		Debug.logMessage("FINE", "Scriptresult created")
	}
	
	setTransactionResults(transactionResults){
		this.transactionResultArray := transactionResults
		Debug.logMessage("FINE", "Transactionresult added in scriptresult")		
	}
	
	getTransactionResults(){
		Debug.logMessage("FINE", "Transactionresults requested")		
		Return this.transactionResultArray
	}
  
}