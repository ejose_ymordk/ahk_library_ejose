Parameter = <empty>
bCheck := false
CheckBox_1 := "no value"

Gui, Add, Tab3,, Check|Convert|Template

Gui, Add, Text, x30 y30 w500 h20 , Selected script:
Gui, Add, Edit, x30 y45 w500 vMyEdit , %Parameter%
Gui, Add, Button, x30 y110 gTest1, Select a File
Gui, Add, Button, x130 y110 gTest2, Check Script
Gui, Add, Button, x480 y110 w50 gGuiClose, Exit


Gui, Tab, Convert
Gui, Add, Text, x30 y30 w500 h20 , Selected script:
Gui, Add, Edit, x30 y45 w500 vMyEdit2 , %Parameter%
Gui, Add, Button, x30 y110 gTest1, Select a File
Gui, Add, Button, x130 y110 gTest3, Convert Script
Gui, Add, Button, x480 y110 w50 gGuiClose, Exit


Gui, Tab, Template
Gui, Add, Text, x30 y30 w500 h20 , Enter template location:
Gui, Add, Edit, x30 y45 w500 vTrans1 , %A_ScriptDir%
Gui, Add, Text, x30 y70 w500 h20 , Enter transaction names seperated by semicolon (;):
Gui, Add, Edit, x30 y85 w500 vTrans2 , 

Gui, Add, Button, x30 y110 gTest4, Create Template.
Gui, Add, CheckBox, x130 y110 vCheckBox_1, Yvalidate template?
Gui, Add, Button, x480 y110 w50 gGuiClose, Exit
Gui, Tab

Gui, Show, , Script checker
error := false
global str
Gui, Submit, NoHide
Return

Test1:

FileSelectFile, Parameter, 3, , Open a file, AHK Script (*.ahk)
if Parameter =
    MsgBox, The user didn't select anything.
else
    MsgBox, The user selected the following:`n%Parameter%
	GuiControl,, MyEdit, %Parameter%
	GuiControl,, MyEdit2, %Parameter%
	bCheck := true
return


GuiClose:
ExitApp

Test2:
if (!bCheck) {
	MsgBox, Select a file Please!
	return
}
	
fileRead,str,%Parameter%

match_main = main\(\)
match_init = init\(\) 
match_finalise = finalise
match_Measurement = new Measurement
match_newTransaction = \.newTransaction
match_start = \.start\(\)
match_stop = \.stop\(\)
match_legacy = srun\.start_transaction
match_parser = parseResult\("LOG"\)
match_pause = pauseTimer\(\)
match_resume = resumeTimer\(\)
match_Loglevel = Debug.setLevel\("FINE"\)

regexReplace( str, "(" match_main ")", match_main, count_main ) 
regexReplace( str, "(" match_init ")", match_init, count_init ) 
regexReplace( str, "(" match_finalise ")", match_finalise, count_finalise ) 
regexReplace( str, "(" match_Measurement ")", match_Measurement, count_Measurement ) 
regexReplace( str, "(" match_newTransaction ")", match_newTransaction, count_newTransaction ) 
regexReplace( str, "(" match_start ")", match_start, count_start ) 
regexReplace( str, "(" match_stop ")", match_stop, count_stop ) 
regexReplace( str, "(" match_legacy ")", match_legacy, count_legacy )
regexReplace( str, "(" match_parser ")", match_parser, count_parser )
regexReplace( str, "(" match_pause ")", match_pause, count_pause )
regexReplace( str, "(" match_resume ")", match_resume, count_resume )
regexReplace( str, "(" match_Loglevel ")", match_Loglevel, count_Loglevel )

count_stop1 := count_stop-1

if (count_main !=2 ) {
	msgbox, 48, Error! ,There are missing Main blocks in the script. There should be two Main() blocks present in the script.`nGo Fix!
	error := true
}
if (count_init !=2 ) {
	msgbox, 48, Error! ,There are missing Init blocks in the script. There should be two Init() blocks present in the script.`nGo Fix!
	error := true
}
if (count_finalise !=2 ) {
	msgbox, 48, Error! ,There are missing finalise blocks in the script. There should be two finalise blocks present in the script.`nGo Fix!
	error := true
}
if (count_Measurement !=1 ) {
	msgbox, 48, Error! ,There is a missing new Measurement block in the script. There should be one new Measurement block present in the script.`nGo Fix!
	error := true
}
if (count_newTransaction > count_start ) {
	msgbox, 48, Error! ,There are more .newTransaction functions than .start functions. There are missing .start functions.`nGo Fix!
	error := true
}
if (count_newTransaction < count_start ) {
	msgbox, 48, Error! ,There are more .start than .newTransaction functions. There are missing .newTransaction functions.`nGo Fix!
	error := true
}
if (count_start > (count_stop1) ) {
	msgbox, 48, Error! ,There are more .start than .stop functions. There are missing .stop functions.`nGo Fix!
	error := true
}
if (count_start < (count_stop1) ) {
	msgbox, 48, Error! ,There are more .stop than .start functions. There are missing .start functions.`nGo Fix!
	error := true
}
if (count_legacy = 0) {
	msgbox, 48, Error! ,The legacy transactions are missing in the script. Add legacy transactions to the script.`nGo Fix!
	error := true
}	
if (count_legacy < count_newTransaction) {
	msgbox, 48, Error! ,There are more transactions than legacy transactions. There are missing legacy transactions in the scripts. Add the missing legacy transactions to the script.`nGo Fix!
	error := true
}
if (count_parser = 0) {
	msgbox, 48, Error! ,There is no parseResult functions present in the script. Add the parseResult function into the Finalise transaction.`nGo Fix!
	error := true
}
if (count_pause < count_resume) {
	msgbox, 48, Error! ,There are more resume functions than pause functions.There are missing pause functions in the script.`nGo Fix!
	error := true
}
if (count_pause > count_resume) {
	msgbox, 48, Error! ,There are more pause functions than resume functions. There are missing resume functies in the script.`nGo Fix!
	error := true
}
if (count_Loglevel = 1) {
	msgbox, 48, Error! ,Incorrect Loglevel, loglevel is still set to Fine.`nGo Fix!
	error := true
}
If ( error = false) {
	MsgBox, 64 ,Good Job!,Well done Sir!`nThe script is correct!
}
return

Test3:
if (!bCheck) {
	MsgBox, Select a file Please!
	return
}

File_conv := FileOpen(Parameter . "_converted.ahk", "w")

i := 0
transactienamen := Object()

Loop, read, %Parameter%
{
	TextLine := A_LoopReadLine

	winwait_c := RegExMatch(TextLine, "winwait_close_fn\(")
	sleep_m := RegExMatch(TextLine, "sleep_msec\(")					
	im_search := RegExMatch(TextLine, "image_search_wait\(")
	im_search_l := RegExMatch(TextLine, "imagelist_search_wait\(")
	im_search_p_l := RegExMatch(TextLine, "image_pattern_list_search_wait\(")
	im_search_s_g := RegExMatch(TextLine, "image_search_wait_gone\(")				
	winwait_o := RegExMatch(TextLine, "winwait_fn\(")
	im_click := RegExMatch(TextLine, "click_image_pattern\(")
	click2 := RegExMatch(TextLine, "click_image\(") 
	click := RegExMatch(TextLine, "image_click\(")
	image_pat := RegExMatch(TextLine, "checkpoint_wrap_image\(")
	New_tr := RegExReplace(TextLine, "start_transaction" , "newTransaction", ncount)
	fin2_tr := RegExReplace(TextLine, "exitcode[\s]+:=[\s]+([[a-zA-Z]+).finish", "$1.stop", ncount3) 
    met1 := RegExReplace(TextLine, "exitcode[\s]+:=[\s]+([[a-zA-Z]+).finish.*", "$1", ncount4) 
	Stop_tr := RegExReplace(TextLine, ".finish" , ".stop", ncount2)
	fin1_tr := RegExMatch(TextLine, ".save_output")	
	fin3_tr := RegExMatch(TextLine, "ExitApp")	

	transnaam := RegExReplace(TextLine, ".*start_transaction\(""(.*)""\).*", "$1" )
	
	RegExMatch(TextLine, "\((.*)\)",str)
	str := % str1
	RegExMatch(str, "],\s*(\d+)",timeout)
	
	timeout := % timeout1
	
	if (winwait_c > 0) {
		str_array := StrSplit(str, ",")  

		timeout := str_array[3]
		timeout := StrReplace(timeout, A_Space)

		win_title := str_array[1]

		new_str = yWinWaitClose(%win_title%,"%timeout%sec") 

		File_conv.WriteLine(new_str)
	} else if (sleep_m > 0 ) {
		timeout := StrReplace(str, A_Space)
		new_str = msleep("%timeout%") 

		File_conv.WriteLine(new_str)		
	} else if (im_search > 0 or im_search_l > 0 or im_search_p_l > 0) {
		RegExMatch(str, "(\[.*\])",images_l)
		imageslist := % images_l1
		str_array := StrSplit(str, ",")
		
		x1 := str_array[3]
		x1 := StrReplace(x1, A_Space)		
		y1 := str_array[4]		
		y1 := StrReplace(y1, A_Space)
		x2 := str_array[5]
		x2 := StrReplace(x2, A_Space)		
		y2 := str_array[6]
		y2 := StrReplace(y2, A_Space)		

		new_str = yWaitForImage(%x1%,%y1%,%x2%,%y2%,%imageslist%,"%timeout%sec") 

		File_conv.WriteLine(new_str)
	} else if (im_search_s_g > 0 ) {
		RegExMatch(str, "(\[.*\])",images_l)
		imageslist := % images_l1

		str := RTrim(str, ")")
		str_array := StrSplit(str, ",")
		
		x1 := str_array[3]
		x1 := StrReplace(x1, A_Space)			
		y1 := str_array[4]
		y1 := StrReplace(y1, A_Space)	
		x2 := str_array[5]
		x2 := StrReplace(x2, A_Space)			
		y2 := str_array[6]
		y2 := StrReplace(y2, A_Space)			

		new_str = yWaitForImageGone(%x1%,%y1%,%x2%,%y2%,%imageslist%,"%timeout%sec") 

		File_conv.WriteLine(new_str)
	} else if (winwait_o > 0) {
		str_array := StrSplit(str, ",")  

		timeout := str_array[3]
		timeout := StrReplace(timeout, A_Space)

		win_title := str_array[1]

		new_str = yWinWait(%win_title%,"%timeout%sec") 

		File_conv.WriteLine(new_str)
	} else if ( im_click > 0 ) {
		str_array := StrSplit(str, ",")
		RegExMatch(str, "(\[.*\])",images_l)
		RegExMatch(str, "\],(.*)", achter)

		achter := % achter1
		achter_array := StrSplit(achter, ",")
		imageslist := % images_l1	
		
		x1 := str_array[2]
		x1 := StrReplace(x1, A_Space)		
		y1 := str_array[3]		
		y1 := StrReplace(y1, A_Space)
		x2 := str_array[4]
		x2 := StrReplace(x2, A_Space)		
		y2 := str_array[5]
		y2 := StrReplace(y2, A_Space)		
		timeout := achter_array[1]
		timeout := StrReplace(timeout, A_Space)
		clickOffsetX := achter_array[2]
		clickOffsetX := StrReplace(clickOffsetX, A_Space)
		clickOffsetY := achter_array[3]
		clickOffsetY := StrReplace(clickOffsetY, A_Space)
		
		new_str = yImageClick(%x1%,%y1%,%x2%,%y2%,%clickOffsetX%,%clickOffsetY%,%imageslist%,"%timeout%sec") 
		File_conv.WriteLine(new_str)
		
	} else if ( click > 0 ) {
		RegExMatch(str, "(\[.*\])",images_l)
		RegExMatch(str, "\],(.*)", achter)

		achter := % achter1
		achter_array := StrSplit(achter, ",")
		imageslist := % images_l1	
	
		timeout := achter_array[3]
		timeout := StrReplace(timeout, A_Space)
		clickOffsetX := achter_array[1]
		clickOffsetX := StrReplace(clickOffsetX, A_Space)
		
		new_str = yImageClick(0,0,A_ScreenWidth,A_ScreenHeight,%clickOffsetX%,10,%imageslist%,"%timeout%sec") 
		File_conv.WriteLine(new_str)
	} else if ( image_pat > 0 ) {
		RegExMatch(str, "(\[.*\])",images_l)
		RegExMatch(str, "\],(.*)", achter)

		str_array := StrSplit(str, ",")
		achter := % achter1
		achter_array := StrSplit(achter, ",")
		imageslist := % images_l1	

		x1 := str_array[4]
		x1 := StrReplace(x1, A_Space)		
		y1 := str_array[5]		
		y1 := StrReplace(y1, A_Space)
		x2 := str_array[6]
		x2 := StrReplace(x2, A_Space)		
		y2 := str_array[7]
		y2 := StrReplace(y2, A_Space)			

		timeout := achter_array[1]
		timeout := StrReplace(timeout, A_Space)
		
		new_str = yWaitForImage(%x1%,%y1%,%x2%,%y2%,%imageslist%,"%timeout%sec") 
		File_conv.WriteLine(new_str)
	} else if ( click2 > 0 ) {
		RegExMatch(str, "(\[.*\])",images_l)
		RegExMatch(str, "\],(.*)", achter)

		str_array := StrSplit(str, ",")
		achter := % achter1
		achter_array := StrSplit(achter, ",")
		imageslist := % images_l1	

		x1 := str_array[2]
		x1 := StrReplace(x1, A_Space)		
		y1 := str_array[3]		
		y1 := StrReplace(y1, A_Space)
		x2 := str_array[4]
		x2 := StrReplace(x2, A_Space)		
		y2 := str_array[5]
		y2 := StrReplace(y2, A_Space)			

		timeout := achter_array[1]
		timeout := StrReplace(timeout, A_Space)
		xoff := achter_array[2]
		xoff := StrReplace(xoff, A_Space)
		yoff := achter_array[3]
		yoff := StrReplace(yoff, A_Space)
		
		
		new_str = yImageClick(%x1%,%y1%,%x2%,%y2%,%xoff%,%yoff%,%imageslist%,"%timeout%sec")
		File_conv.WriteLine(new_str)

	} else if (ncount > 0) {
		transactienamen[i] := transnaam
		i := i +1
		New_tr := New_tr . "`ntrans.start()`n"
		File_conv.WriteLine(New_tr)
	} else if (ncount2 > 0 && ncount3 = 0) {
		File_conv.WriteLine(Stop_tr)
	} else if (fin1_tr > 0 or fin3_tr > 0){
		File_conv.WriteLine("")	
	} else if (ncount3 > 0) {
		File_conv.WriteLine(fin2_tr)
		Str_n := met1 . ".parseResult(""LOG"")"
		File_conv.WriteLine(Str_n)
	} else {
		File_conv.WriteLine(TextLine)
	}
}
for index, element in transactienamen 
{ 
	trns_naam := "srun.start_transaction(""" . element . """)"
	File_conv.WriteLine(trns_naam)

}

File_conv.Close
MsgBox, 64 ,"Converting complete!","The script is converted. Please check your script, this since converting is not 100`% accurate!"

return

Test4:
Gui, Submit, NoHide
Transactions := ""
StringSplit, Transactions, Trans2,`;


loop, %Transactions0%
{
	Template2 := Template2 .  "		" . Transactions%A_Index% . "(measurement) `; We use number so it is easier to sort when using admin or creating dashboards when script has been uploaded`n"

	Template4 := Template4 . Transactions%A_Index% . "(measurement)`n{`n	trans := measurement.newTransaction(""" . Transactions%A_Index% . """) `; Declare a new transaction within the measurement.`n	trans.start() `; When starting a transaction the timer will also be started automatically.`n	`; insert transaction steps`n	trans.stop() `; Stop the timer and the transaction.`n}`n`n"
	
	Template6 := Template6 . "trans := srun.start_transaction(""" . Transactions%A_Index% . """) `; Points in name are not Allowed.`n"

}

; if yvalidate is selected add yvalidate to template.
if(CheckBox_1==1){
	template = #include ymorlib\Ymorlib.ahk `; include the ymor lib so we can use the ymor functions.`n#NoEnv  `; Recommended for performance and compatibility with future AutoHotkey releases.`nSendMode Input  `; Recommended for new scripts due to its superior speed and reliability.`nSetWorkingDir `%A_ScriptDir`%  `; Ensures a consistent starting directory.`nSetTitleMatchMode, 2 `; A window's title can contain WinTitle anywhere inside it to be a match.`nCoordMode, Mouse, Relative `; Coordinates are relative to the active window.`nglobal totalscripttime, sUsername, sPassword `; , params `; Declare global variables for use in all transactions. totalscripttime is for total script time. sUsername and sPassword are used in paramsCSV file.`n`; --- below are variables which will be used to send results to the specified splunk environment using the Ymor Yvalidate app. ---`nglobal yCreateDocumentation := false `; boolean that is used to create the Ymor script documentation i.e. Designdocument`nglobal ySplunkSend := false `; boolean; true sends results to splunk, false will only store results local.`nglobal yTest := regexreplace(A_scriptname,"\..*","") `; string; specifies the name of the test in Splunk, default is the script name.`nglobal yRun := "Loadtest_1" `; string; specifies the name of the Run in Splunk, change this when multiple runs are needed.`nglobal yScript := "Script_1" `; string; specifies the name of the Script in Splunk, change this when multiple scripts are used in 1 test.`nglobal yValidateSaveScreenshot := false `; boolean; true creates an Yvalidate folder in your current script dir where it saves screenshots on error during the loadtest.`nglobal SplunkAdress := "http://127.0.0.1:9998" `; string; specify address and port of splunk, default yvalidate splunk port is 9998 `nglobal CustomSplunkString := "" `; "ExampleVar=" . ExampleVariabele . ";" `; string(optional); send extra data to splunk, like usernames/passwords used during a scriptrun, useful for analysis, allways end with semicolon.`n`; --- above are variables which will be used to send results to the specified splunk environment using the Ymor Yvalidate app. ---`n`nmain() `; Start function main, which basically starts and runs the script we define below.`n`nmain() `; Main function`n{`n	measurement := init() `; First run the init`n	try `; Try catch so if a transaction fails the script will stop but will still finish the transaction itself.`n	{`n		
} else {
	template = #include ymorlib\Ymorlib.ahk `; include the ymor lib so we can use the ymor functions.`n#NoEnv  `; Recommended for performance and compatibility with future AutoHotkey releases.`nSendMode Input  `; Recommended for new scripts due to its superior speed and reliability.`nSetWorkingDir `%A_ScriptDir`%  `; Ensures a consistent starting directory.`nSetTitleMatchMode, 2 `; A window's title can contain WinTitle anywhere inside it to be a match.`nCoordMode, Mouse, Relative `; Coordinates are relative to the active window.`nglobal yCreateDocumentation := false `; boolean that is used to create the Ymor script documentation i.e. Designdocument`nglobal totalscripttime, sUsername, sPassword `; , params `; Declare global variables for use in all transactions. totalscripttime is for total script time. sUsername and sPassword are used in paramsCSV file. `n`nmain() `; Start function main, which basically starts and runs the script we define below.`n`nmain() `; Main function`n{`n	measurement := init() `; First run the init`n	try `; Try catch so if a transaction fails the script will stop but will still finish the transaction itself.`n	{`n	
}

Template3 := "	} catch e {`n		Debug.logMessage(""ERROR"", ""Exception in: "" e.File "", linenumber: "" e.Line "", errormessage: "" e.Message "", function: "" e.What ) `; Log to logger in Scite.`n	}`n	finalise(measurement) `; Stop the measurement and parse the results.`n}`n`ninit() {`n	Debug.setLevel(""SEVERE"") `; Since this is the test template script we will set the debug level to log everything. Allowed levels are`; FINE, CONFIG, INFO, WARNING, SEVERE. Try them all. :)`n	FileFunctions.ClearLogFolder() `; Clear the logfiles in the output2 folder. This can be useful when making a script and you want a new logfile for each run.s`n	measurement := new Measurement(""YDummyScriptName"",900) `; Start a new measurement with a timeout of 900 seconds which is 15 minutes. This is the default value. Input you set is seconds.`n	measurement.setImageDir(""\Images\"") `; Set the imagedir. Currently not adjustable.`n    `;~ transaction.yKillAllExcept([""""],[""""],true) `; Command that will kill all open programs. This is a Testrun, which will show message boxes of all open programs and what will happen to each program (kill or no kill).`n	`;~ params := FileFunctions.ReadCSV(""params.csv"", A_ComputerName) `; Read sentinel dependent parameters from a csv file and place in variable.`n	`;~ sUsername := params[""sUsername""] `; Get username from the file and save to variable for later use.`n    `;~ sPassword := params[""sPassword""] `; Get password from the file and save to variable for later use.`n	return measurement`n}`n`n"

Template5 = finalise(measurement) {`n	measurement.stop() `; Stop the measurement`n	measurement.parseResult("LOG") `; Parse the results to the logfile and result file`n}`n`n`;------------------------------------------ DECLARE TRANSACTIONS FOR UPLOADING ------------------------------------------`;`n`; This is necessary so that the transactions are being detected when uploading in the admin.`n

if FileExist(Trans1 . "\Template.ahk") {
	MsgBox, 64 , "Warn! Template.ahk will be overwritten!","The template.ahk already exists and will be overwritten. If you want to keep the old template.ahk rename it first."
	FileDelete, Trans1 . "\Template.ahk"
}

File_conv := FileOpen(Trans1 . "\Template.ahk", "w")

File_conv.Write(template . Template2 . Template3 . Template4 . Template5 . Template6)
File_conv.Close
MsgBox, 64 ,Template created!,The template can be found at %Trans1%\Template.ahk
Reload
GuiControl,, Trans2,
return