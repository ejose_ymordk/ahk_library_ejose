﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

/**
 * Class that is used to handle the transactionresults of a measurement.
 *
 */

class TransactionResult{
	
	duration := ""
	startDate := ""
	endDate := ""
	errorResult := ""
	name := ""
	
	__New(name) {
		this.name := name
	}
	
	; @doc Set for transaction duration in seconds
	setDuration(duration) {
		this.duration := duration
	}
	
	; @doc Get for transaction duration in seconds
	getDuration() {
		return this.duration
	}
	
	; @doc Set the transaction startdate
	setStartDate(startDate){
		this.startDate := startDate
	}
	
	; @doc Get the transaction startdate
	getStartDate(){
		return this.startDate
	}
	
	; @doc Set the transaction name
	setName() {
		this.name := name
	}
	
	; @doc Get the transaction name
	getName() {
		return this.name
	}
	
	; @doc Set the transaction error result
	setErrorResult(errorResult) {
		this.errorResult := errorResult
	}
	
	; @doc Get the transaction error result
	getErrorResult(){
		return this.errorResult
	}
}
