﻿; Include the lib files this class needs.   
#include ymorlib\Debug.ahk
#include ymorlib\TransactionResult.ahk
#include ymorlib\ErrorResult.ahk
#include ymorlib\Measurement.ahk
#include ymorlib\ScreenCapture.ahk
#include ymorlib\FileFunctions.ahk
#include ymorlib\Notification.ahk

#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
Global Runtime
Global TN
Global Plaats

/**
 * Transaction class, unit of measurements to present in Ymonitor.
 * 
 * Custom Ymor functions for usage in transactions are being declared here.  
 */
 
class Transaction {
	
	; Declare variables.
	name := ""
	timerStart := ""
	timerStop := ""
	transactionTime := 0
	startDate := 0
	transactionResult := ""
	errorResult := ""
	continueOnError := false
	transactionStopping := false
	transactionStarting := false
	transactionResults := Object()
	imageArrayList := Object()
	imagesPatternList := Object()
	returnValue := Object()
	imageslist := ""
	ErrorOccured := false
	transactionTimeOverride := false
	transactionPaused := false
	Runtime := 0
	scriptVersion := 0
	enableScrenCapForDoc := false

	; Build constructor.
	__New(tName) {
		this.fileLocation := A_ScriptDir . "\output2"
		this.name := tName
		TN := tName
		this.Measurement.checkTime()
		SplitPath, A_ScriptDir, bname
		this.scriptVersion := bname
		if(this.enableScrenCapForDoc = true) {
			yDesigndocumentFolder := A_ScriptDir . "\yDesigndocument"
			if !FileExist(yDesigndocumentFolder) { 
				FileCreateDir, %yDesigndocumentFolder%
			}
		}
	}
	
	; @doc Ymor Command - ySaveScreenshot command takes a screenshot of the current screen. It's Used when error is encountered during a measurement, and could also be used as a custom command to take an extra screen.
	; @param aRect, If aRect is 0/1/2/3, captures the entire desktop/active window/active client area/active monitor.
	; @param bCursor, is True, captures the cursor too.
	; @param sFile, if 0, set the image to Clipboard. If it is omitted or "", saves to screen.bmp in the script folder, otherwise to sFileTo which can be BMP/JPG/PNG/GIF/TIF.
	; @param nQuality, is applicable only when sFileTo is JPG. Set it to the desired quality level of the resulting JPG, an integer between 0 - 100.
	ySaveScreenshot(aRect = 0, bCursor = false, sFile = "", nQuality = "") {
		Debug.logMessage("INFO", "capturing screen to: " . sFile . " in directory: " . A_ScriptDir . "\output2")
		CaptureScreen(aRect, bCursor, this.fileLocation . "\" . sFile)   
	}
	
	ySaveScreenshotYvalidate(aRect = 0, bCursor = false, sFile = "", nQuality = "") {
		Debug.logMessage("INFO", "capturing screen to: " . sFile . " in directory: " . A_ScriptDir . "\yvalidate")
		CaptureScreen(aRect, bCursor, sFile)   
	}

	ySaveScreenshotDesigndocument(aRect = 0, bCursor = false, sFile = "", nQuality = "") {
		Debug.logMessage("INFO", "capturing screen to: " . sFile . " in directory: " . A_ScriptDir . "\yDesigndocument")
		CaptureScreen(aRect, bCursor, sFile)   
	}

	; @doc Check if an error has occured, if so throw an exception.
	checkErrorOccured() {
		if(this.ErrorOccured){
			throw Exception("Script has encountered and error, finishing...", -1)
		}
	}
  
	; @doc Log an errormessage when a measurement encounters an error, also take a screenshot of the entire desktop and set the resultstatus of the transaction to 0. 
	; @param message, the message that is written to the logfile.
	error(message) {
		Debug.logMessage("TRANS", "Transaction " . this.name . " has triggered an error, the errormessage was: " . message)
		this.errorResult.setResultStatus("0")
		this.errorResult.setMessage(message)
		this.ySaveScreenshot(0, false, this.name . ".png")
		; Set error message after taking screenshot, this is because of backwards compatibility with the Ymonitorclient parser.
		Debug.logMessage("ERROR", "Transaction " . this.name . " has triggered an error, the errormessage was: " . message) 
		if(!this.continueOnError) {
			this.ErrorOccured := true
			this.stop()
		}
	}
	
	; @doc Function show a notification msg
	notify(msg) {
		Notification.show(this.name . "(" . this.scriptVersion . "): " . msg)
	}
	
	; @doc Function to start a transaction in a measurement.
	start() {
		if(this.enableScrenCapForDoc = true) {
			this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "_start.png")
			Sleep, 1000
		}
		
		this.notify("started")
		this.checkErrorOccured()
		this.transactionStarting := true
		this.yCreateDocumentation()
		Debug.logMessage("TRANS", "-------------------------------------------------------------------- START OF TRANSACTION -----------------------------------------------------------------------")
		Debug.logMessage("TRANS", "Transaction started: " . this.name . "")
		this.transactionStarting := false
		this.transactionResult := New TransactionResult(this.name)
		this.transactionResults.Insert(this.transactionResult)
		this.errorResult := new ErrorResult(this.name)
		this.errorResult.setResultStatus("1")
		this.yStartTimer()

	}
	
	; @doc Function to stop transaction in a measurement.
	stop() {
		if(this.enableScrenCapForDoc = true) {
			this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "_stop.png")
			Sleep, 1000
		}
		
		this.notify("stop")
		; Set this for correct closing of transactions. 	
		this.transactionStopping := true
		
		this.yStopTimer()
		this.transactionResult.setDuration(0.001 * this.TransactionTime)
		totalscripttime := this.transactionResult.getDuration() + totalscripttime
	
		if (totalscripttime = "") {
			Debug.logMessage("TRANS", "Transaction finished: " . this.name . ", success: 0, transaction time (sec): 0.31337")			
		}
		else {
			Debug.logMessage("TRANS", "Transaction finished: " . this.name . ", success: " . this.errorResult.getResultStatus() . ", transaction time (sec): " this.transactionResult.getDuration() . "")
		}

        Debug.logMessage("TRANS", "-------------------------------------------------------------------- END OF TRANSACTION -------------------------------------------------------------------------")
		;this for sending result to splunk, only if set in script.
		this.ySplunkSend()	
		this.transactionStopping := false
		this.transactionResult.setErrorResult(this.errorResult)

		TN := ""
	
		this.checkErrorOccured()	
	}
	
	; @doc Start the timer. This is used when starting a transaction
	; @doc A_Tickcount is the number of milliseconds since the computer was rebooted. By storing A_TickCount in a variable, elapsed time can later be measured by subtracting that variable from the latest A_TickCount value.
	yStartTimer() {
		if(!this.transactionStarting){
			Debug.logMessage("INFO", "Starting Timer")
		}
		if(this.timerStart != "" ){
			Debug.logMessage("INFO", "Timer was already started, resetting start time")
		}
		this.timerStart := A_TickCount
	}
	
	; @doc Stop the timer. This is used when stopping a transaction.
	yStopTimer() {
		this.timerStop := A_TickCount
		if(!this.transactionStopping){
			Debug.logMessage("INFO", "Stopping Timer")
		}	
		if (this.timerStart = "") {
			if(!this.transactionStopping) {
				Debug.logMessage("INFO", "Incorrect use of the startTimer and stopTimer functions")
			}
		} else {
			if (this.transactionTimeOverride) {
				Debug.logMessage("INFO", "Transactiontime has been set in script, using value set in script.")
			}
			else{
				if (transactionPaused) {
					this.yResumeTimer()
				}
				this.transactionTime := this.transactionTime + (this.timerStop - this.timerStart)
				this.timerStart := ""
			}
		}
	}
	
	yResetTimer() {
		if(this.enableScrenCapForDoc = true) {
			this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "_reset.png")
			Sleep, 1000
		}
		this.notify("reset timer")
		Debug.logMessage("INFO", "Timer reset")
		this.transactionTime := 0
		this.timerStart := A_TickCount
		this.timerStop := this.timerStart
	}
	
	; @doc Ymor Command - yPauseTimer is a command used to pause the timer within a transaction. Used to perform tasks which should not be added to the transaction time. Should be followed by yResumeTimer. 
	yPauseTimer(){
		if(this.enableScrenCapForDoc = true) {
			this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "_pause.png")
			Sleep, 1000
		}
		this.notify("pause timer")
		this.timerStop := A_TickCount
		this.transactionPaused := true
		Debug.logMessage("INFO", "Pausing Timer")
	 
		if (this.timerStart = "") {
			Debug.logMessage("INFO", "Incorrect use of the startTimer and stopTimer functions.")
		} else {
			this.transactionTime := this.transactionTime + (this.timerStop - this.timerStart)
			this.timerStart := ""
		}
	}
	
	; @doc Ymor Command - yResumeTimer is a command used to resume the timer within a transaction and is used to perform tasks which should not be added to the transaction time. Must be used after yPauseTimer.
	yResumeTimer() {
		if(this.enableScrenCapForDoc = true) {
			this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "_resume.png")
			Sleep, 1000
		}
		this.notify("resume timer")
		Debug.logMessage("INFO", "Resuming Timer")
		this.timerStart := A_TickCount
		this.transactionPaused := false
	}
	
	; @doc Ymor Command - ySleep is a custom Sleep function where the sleep time is not being added to the transaction time. 
	ySleep(sleeptime) {
		
		; Set all timeout modes to milliseconds
		if sleeptime is digit
		{
			timeoutSec := sleeptime
			Debug.logMessage("INFO", "No sleep annotation used (msec/sec/min) as default msec is used. Please change to trans.ySleep(" . sleeptime . "msec).")
		}
		else if (RegExMatch(sleeptime, "i)^(\d*)\w*msec\w*", match))  {
			timeoutSec := match1
		} 
		; From seconds
		else if (RegExMatch(sleeptime, "i)^(\d*)\w*sec\w*", match)) {
			timeoutSec := match1 * 1000
		} 
		; From minutes
		else if (RegExMatch(sleeptime, "i)^(\d*)\w*min\w*", match)) {
			timeoutSec := match1 * 60000
		} 
		else 
		{
			Debug.logMessage("SEVERE", "Geen geldige input gevonden voor timeout. Opgegeven timeout was: '" . sleeptime "' . Het script wordt gestopt.")
			Measurement.Halt()
		}
		
		if (this.transactionPaused) {	; Check is transaction is already paused.
			Debug.logMessage("INFO", "Input was " . sleeptime . " so the transaction is now sleeping for " . timeoutSec . " miliseconds. Sleeptime is not added to the transaction time.")
			Sleep, timeoutSec		
		} else {
			this.yPauseTimer()
			Debug.logMessage("INFO", "Input was " . sleeptime . " so the transaction is now sleeping for " . timeoutSec . " miliseconds. Sleeptime is not added to the transaction time.")
			Sleep, timeoutSec
			this.yResumeTimer()
		}
	}

	; @doc Ymor Command - ySetTransactionTime sets a custom transaction time. Can be used to alter transaction times in the measurement, i.e. synthetic transactions. 
	; @param transactionTime=""
	ySetTransactionTime(transactionTimeMS=1000) {
		Debug.logMessage("INFO", "Transaction Time has been set in script to " . transactionTimeMS . " miliseconds.")
		this.transactionTimeOverride := true
		this.transactionTime := transactionTimeMS
	}
	
	; @doc Setter for start date
	setStartdate() { 
		this.startDate := startDate
	}
	
	; @doc Get the total transactiontime.  
	getTransactionTime(){
		if (this.transactionTimeOverride){
			tempTransTime := this.transactionTime
		}
		else {
			tempTransTime := this.transactionTime + (this.timerStop - this.timerStart)
		}
		Debug.logMessage("INFO", "Transactiontime requested, transaction is running for " . tempTransTime . " miliseconds")
		return tempTransTime
	}
	
	; @doc Getter for transaction name. 
	getName() {
		return this.name
	}
		
	; @doc Set the transactions results.  
	setTransactionResults(TransactionResults) {
		for index, transactionResult in transactionResults {
			transactionResult.setName(this.name)
			this.transactionResults.Insert(transactionResult)
		}
	}
	
	; @doc Get the transaction results. 
	getTransactionResults(){
		Debug.logMessage("FINE", "Transactionresult requested")	
		return this.transactionResults
	}
	
	; @doc Set the imagedir. 
	setImageDir(imageDir) {
		this.imageDir := imageDir
	}
	
	; @doc Get the imagedir.
	getImageDir() {
		return A_ScriptDir . this.imageDir
	}
	
	; @doc Find images based on pattern on filesystem (example -> test*.png).
	findImageByPattern(imagepattern) {
		res := Array()
		location := A_ScriptDir . this.imageDir . imagepattern
	
		Loop Files, %location%
		{
			res.Insert(A_LoopFileFullPath)
		}
		return res
	}
	
; -------------------------------------------------------------------------- Ymor Custom Functions -------------------------------------------------------------------------- ; 
		
	; @doc Ymor Command - yWinWaitClose command waits for a custom window to dissappear.
	; @param wintitle, A window title or other criteria identifying the target window.
	; @param timeout, the timeout is msec, sec or mins. Default is 60 seconds.
	; @param ThrowError, Throw an error when the custome window is not closed within timeout. Default is true.
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	yWinWaitClose(wintitle, timeout="60sec", ThrowError=true, customErrorMessage="") {
		
		; Set all timeout modes to seconds.
		; From milliseconds
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutSec := match1 / 1000
		} 
		; From seconds
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match)) {
			timeoutSec := match1
		} 
		; From minutes
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match)) {
			timeoutSec := match1 * 60
		} 
		else 
		{
			Debug.logMessage("SEVERE", "Geen geldige input gevonden voor timeout. Opgegeven timeout was: '" . timeout "' . Het script wordt gestopt.")
			Measurement.Halt()
		}
		
		WinWaitClose, %wintitle%,,%timeoutSec%
		if (errorlevel=0)
		{ 
			Debug.logMessage("INFO", "Window " . wintitle . " closed within " . timeoutSec . " seconds")
		}
		else if (errorlevel=1)
		{
			Debug.logMessage("SEVERE", "Window was not closed within specified timeout.")
			if (ThrowError)
			{
				if(customErrorMessage != "")
				{
					this.error(customErrorMessage) 
				} else {
					this.error(result . " Could not find window") 
				}
			}
		}
	
	}

	; @doc Ymor Command - yWinWait command waits for a custom window to appear.
	; @param wintitle, A window title or other criteria identifying the target window.
	; @param timeout, the timeout is msec, sec or mins. Default is 60 seconds.
	; @param ThrowError, Throw an error when the custome window is not closed within timeout. Default is true.
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	yWinWait(wintitle, timeout="60sec", ThrowError=true, customErrorMessage="") {
		
		; Set all timeout modes to seconds.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutSec := match1 / 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match)) {
			timeoutSec := match1
		}
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match)) {
			timeoutSec := match1 * 60
		} 
		else 
		{
			Debug.logMessage("SEVERE", "Geen geldige input gevonden voor timeout. Opgegeven timeout was: '" . timeout "' . Het script wordt gestopt.")
			Measurement.Halt()
		}
		
		WinWait, %wintitle%,,%timeoutSec%
		if (errorlevel=0) 
		{ 
			Debug.logMessage("INFO", "Window " . wintitle . " found within " . timeoutSec . " seconds")
		}
		else if (errorlevel=1) 
		{
			Debug.logMessage("SEVERE", "Window " . wintitle . " found within " . timeoutSec . " seconds")
			if (ThrowError)
			{
				if(customErrorMessage != "")
				{
					this.error(customErrorMessage) 
				} else {
					this.error(result . " Window " . wintitle . " found within " . timeoutSec . " seconds")
				}
			}			
		}

	}

	YZoek_Index(haystack, needle) {
		if !(IsObject(haystack)) || (haystack.Length() = 0)
		{
			return 0
		}
		for index, value in haystack
		{
			word_array := StrSplit(value, "*.png") ; Omits periods.
			word := word_array[1]
		
			if InStr(needle,word)
			{
				Plaats := index
				return index
			}
		}
		return 0
	}

	; @doc Custom Ymor imagecheck function which checks if an image can be found on the screen. 
	; @param x1/y1, The X and Y coordinates of the upper left corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param x2/y2, The X and Y coordinates of the lower right corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param imageslist, The list of images that will be checked for on the screen.
	; @param timeout, The timeout in msec, sec or mins. Default is 60 seconds.
	; @param ThrowError, Throw an error when the image can't be found with the timeout. Default is true. 
	; @param options, Specify for options a number between 0 and 255 (inclusive) to indicate the allowed number of shades of variation in either direction for the intensity of the red, green, and blue components of each pixel's color. For example, *2 would allow two shades of variation. Default is 5.
	; @param gone, This is necessary for the YwaitforImage and yWaitforImageGone because we use the yImagecheck in these functions. Default is false. Should not be adjusted.  
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	; @return returnvalue, Return the found image index, x coordinate, y coordinate, message, errorcode and element.
	yImageCheck(x1, y1, x2, y2, imageslist, timeout="60sec", ThrowError=true, options="*5", gone=False, customErrorMessage="") {
		this.imageArrayList := Object() 		; Clear the global imageArrayList.
		this.imageFound := false 				; Clear the global imageFound boolean.
		this.imagesPatternList := Object() 		; Clear the global imagePatternList.
		
		this.returnValue := Array() 			; An array will be returned which contains the information about the first image element and x and y coördinates found.
		this.result := Array()					; Clear the result array.
		this.reverseImageArrayList := Object()	; Array to search in reverse order.
		
		Debug.logMessage("FINE", "Searching image files in: " . A_ScriptDir . this.imageDir)
		
		; Set all timeout modes to msec.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {

			timeoutSec := match1 / 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match))  {
			
			timeoutSec := match1
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match))  {

			timeoutSec := match1 * 60
		} 
		else 
		{
			Debug.logMessage("SEVERE", "Geen geldige input gevonden voor timeout. Opgegeven timeout was: '" . timeout "' . Het script wordt gestopt.")
			Measurement.Halt()
		}
		
		; Store images in an array.
		this.imagesPatternList := imageslist
		max := timeoutSec * 1000
		; Create timerstop where we count timeout in milliseconds + tickcount 
		timerStop := A_TickCount + max
		
		; Below is used when there is a pattern used in the imagecheck for example (test*.png) 
		For index, element in this.imagesPatternList {
			; Search for astrix, if found activate based on pattern search
			searchPattern = *
			IfInString, element, %searchPattern%
			{
				basedOnPattern := true
			}
			else {
				basedOnPattern := false
			}

			res = array()
			imageexists := element
			; If pattern is found then loop through files with same name. 
			if(basedOnPattern){
				res := this.findImageByPattern(element)
			}
			else {
				res := [A_ScriptDir . this.imageDir . element]
			}
			for index , element in res {
				Debug.logMessage("FINE", "Image: " . element . " found in imagelist")
				this.imageArrayList.Insert(element)
			}
		}
		
		; Length is 0 when no images are found since we delete every element from the array. 
		for index, element in this.imageArrayList {
			if !FileExist(element) { 
				 this.imageArrayList.removeAt(index)
			 }
		}
		
		MinIndex := this.imageArrayList.Length()
		reverse_count := this.imageArrayList.Length()
		Org_index := MinIndex + 1
		
		if (MinIndex = 0) {
			; Give an warning when no images are found in the imageDir, this will trigger an error when ahk version is < 1.1.21.
			transName := this.getName()
			Debug.logMessage("SEVERE", "The specified image " . imageexists . " in transaction " . transName . " could not be found in image directory. Check if images that you use exist and have the correct name.") 
			; Give an specific pop-up for the user if the images are not in the imagedir. Specify also the transaction in which this occurs. 
			MsgBox, The specified image: "%imageexists%" in transaction "%transName%" cannot be found. Make sure the image exists and has the correct name in the image directory. The script will now stop!
			
			if(ThrowError) {
				this.error("No images found in imageDir. Check if images that you use in yImagecheck are there.")
				this.imageFound := true					
			}
			else {
				this.imageFound := true
			}
		}
		
		; Reverse the order of the imageArrayList. When a pattern is used in the image search it will search the last image found with the pattern else search the first image.
		for index, element in this.imageArrayList {
			this.reverseImageArrayList[reverse_count] := element
			reverse_count := reverse_count - 1
		}
		
		; Move the move to topleft of the active screen before we search for a image. 
		MouseMove, 0 ,0, 0
		; While current tickcount is smaller then ( timeout + tickcount ) and image has not been found -> loop.
		image_loop:
		while (!this.imageFound) {
			; This is a variable we use so that the images we check we check them as a series instead of seperate. 
			checkseries := 0
			for index, element in this.reverseImageArrayList {
				; Strip the imagelocation, this is only for the logfile so it is better readable. We only want to log the image we search for and not the complete path.
				imagelocation := element
				argument := ".*\\"
				strippedimagelocation := RegExReplace(imagelocation, argument)
				; If the image has been found then break from while.
				if(this.imageFound) {
					break
				}
				else 
				{	
					; Images exist in imageDir so continue and create imagepath.
					imageToCheck := element
					ImageSearch,FoundX, FoundY,%x1%,%y1%,%x2%,%y2%,%options% %imageToCheck%
						; If image has been found return true. 
						if (ErrorLevel = 0) {
							this.YZoek_Index(this.imagesPatternList, imageToCheck)
							
							index := Org_index - index
							Debug.logMessage("INFO", "Image: " . strippedimagelocation . " found at x: " . FoundX " y: " . FoundY)
							this.imageFound := true
							ReturnMessage := "Image was found on screen within specified timeout"
							errorcode := 1 ; Image is found.
							checkseries := checkseries or 1
							
							this.returnValue.Insert(Plaats) ; Add the imagepath
							this.returnValue.Insert(FoundX)  ; Add x coördinate
							this.returnValue.Insert(FoundY)  ; add y coördinate
							this.returnValue.Insert(ReturnMessage)  ; add the return message
							this.returnValue.Insert(errorcode) ; 1 is good. 
							this.returnValue.Insert(element) ;	image element									
							
							if(this.enableScrenCapForDoc = true) {
								this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "-" . strippedimagelocation . "_check.png")
							}
						}
						; This is useful to see how long it takes to find the image. 
						else if (ErrorLevel = 1) {
							checkseries := checkseries or 0
						
							if (index = MinIndex) {
								if (gone and not checkseries) {		
									this.imageFound := true
									errorcode := 3 ; Image is not found.
									
									this.returnValue.Insert(index) 
									this.returnValue.Insert(FoundX)  
									this.returnValue.Insert(FoundY)  
									this.returnValue.Insert(ReturnMessage)  
									this.returnValue.Insert(errorcode)  
									this.returnValue.Insert(element) 	
								}
							}
							Debug.logMessage("FINE", "Image: " . strippedimagelocation . " not found this time.")
						}		
						; If there was a problem that prevented the command from conducting the search
						else if (ErrorLevel = 2) {
							Debug.logMessage("INFO", "Image: " . strippedimagelocation . " could not be opened or is badly formatted. Please check if image exists or is corrupt.")
							this.reverseImageArrayList.Delete(index)
						}
						; If someone adjusted the ImageSearch function. This should not occur.
						else {
							Debug.logMessage("SEVERE","Please check imageSearch function, don't call the AHK guru's, please download supported libary.")
						}
						
						; Check if timeout has been reached.
						if(A_tickCount > timerStop) {
							Debug.logMessage("INFO", "Image: " . strippedimagelocation . " not found at specified coördinates.")
							for index, element in this.reverseImageArrayList {
								imageToCheck := element
								ImageSearch,FoundX,FoundY,0,0,A_ScreenWidth,A_ScreenHeight,%options% %imageToCheck%
								if (ErrorLevel = 0) {
									this.YZoek_Index(this.imagesPatternList, imageToCheck)
									index := Org_index - index
									Debug.logMessage("INFO", "Image: " . strippedimagelocation . " found at x: " . FoundX " y: " . FoundY)
									ReturnMessage := "Image was found on whole screen but not on specified coördinates"
									errorcode := 2 ; Not found directly but found on whole screen.  
									this.returnValue.Insert(Plaats) 
									this.returnValue.Insert(FoundX)  
									this.returnValue.Insert(FoundY)  
									this.returnValue.Insert(ReturnMessage)  
									this.returnValue.Insert(errorcode)  
									this.returnValue.Insert(element) 										
									break, image_loop									
								}
								else if (ErrorLevel = 1) {
									Debug.logMessage("FINE", "Image: " . strippedimagelocation . " also not found on whole screen.")
									errorcode := 3 ; Not found on whole screen. 		
									ReturnMessage := "Image " . strippedimagelocation . " also not found on whole screen."
									; If images could not be found set index to 0, ergo result is 0. 
									this.returnValue.Insert(0) 
									this.returnValue.Insert(FoundX)  ; empty
									this.returnValue.Insert(FoundY)  ; empty
									this.returnValue.Insert(ReturnMessage)  
									this.returnValue.Insert(errorcode) 	
									this.returnValue.Insert(element) 
									if (ThrowError) {
										if(customErrorMessage != "")
										{
											this.error(customErrorMessage) 
										} else {
											; Match all characters that are not a "\\" starting from the end of the imagepath
											argument := "[^\\]+$"      
											RegExMatch(imageToCheck, argument, result)
											this.error(result . " not found on screen.") 
										}										
									}									
									break, image_loop	
								}
								else {
									; If someone adjusted the ImageSearch function. This should not occur.
									Debug.logMessage("SEVERE","Please check imageSearch function, don't call the AHK guru's, please download supported libary.")
									break, image_loop
								}
							}
						}
						else {
							; nothing to see here continue.
						}
				} ; end of else imagefound.
			} ; end of for.
		} ; end of while.
		
		this.imageArrayList := Object() 	   ; Clear the global imageArrayList
		this.reverseImageArrayList := Object() ; Clear the global reverseImageArrayList
		this.imageFound := false 			   ; Clear the global imageFound boolean
		this.imagesPatternList := Object()     ; Clear the global imagePatternList
		
		return this.returnValue ; Return the found index, x coordinate, y coordinate, message, errorcode and element. 
	} ; end of function.
	
	; @doc Ymor Command - yWaitForImage is a custom imagecheck command which waits for an image to appear.
	; @param x1/y1, The X and Y coordinates of the upper left corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param x2/y2, The X and Y coordinates of the lower right corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param imageslist, The list of images that will be checked for on the screen.
	; @param timeout, The timeout in msec, sec or mins. Default is 60 seconds.
	; @param ThrowError, Throw an error when the image can't be found with the timeout. Default is true. 
	; @param options, Specify for options a number between 0 and 255 (inclusive) to indicate the allowed number of shades of variation in either direction for the intensity of the red, green, and blue components of each pixel's color. For example, *2 would allow two shades of variation. Default is 5.
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	; @return res, return the first image found in the array.
	yWaitForImage(x1, y1, x2, y2, imageslist, timeout="60sec", ThrowError=true, options="*5", customErrorMessage="") {
		
		; Set all timeout modes to msec.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutSec := match1 / 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match))  {			
			timeoutSec := match1
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match))  {
			timeoutSec := match1 * 60
		} 
		else 
		{
			Debug.logMessage("SEVERE", "No valid input for timeout. Timeout was: '" . timeout "' . The script will now stop.")
			Measurement.Halt()
		}
		
		res := Array()
		Debug.logMessage("INFO", "Waiting to find image files in: " . A_ScriptDir . this.imageDir)
		res := this.yImageCheck(x1, y1, x2, y2, imageslist, timeout, ThrowError, options, false, customErrorMessage)
		errorcode := res[1]
		Debug.logMessage("INFO", "Returning position in array: " . res[1])
		return errorcode
	} ; end of function
	
	; @doc Ymor Command - yWaitForImageGone command is a custom imagecheck command which waits for an image appear and then disappear.
	; @param x1/y1, The X and Y coordinates of the upper left corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param x2/y2, The X and Y coordinates of the lower right corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param imageslist, The list of images that will be checked for on the screen.
	; @param timeout, The timeout in msec, sec or mins. Default is 60 seconds.
	; @param ThrowError, Throw an error when the image is still found with the timeout. Default is true. 
	; @param options, Specify for options a number between 0 and 255 (inclusive) to indicate the allowed number of shades of variation in either direction for the intensity of the red, green, and blue components of each pixel's color. For example, *2 would allow two shades of variation. Default is 5. 
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	yWaitForImageGone(x1, y1, x2, y2, imageslist, timeout="60sec", ThrowError=true, options="*5", customErrorMessage="") {
		image_Error := Object()
		
		; Set all timeout modes to msec.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutMsec := match1 
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match))  {
			timeoutMsec := match1 * 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match))  {
			timeoutMsec := match1 * 60000
		} 
		else 
		{
			Debug.logMessage("SEVERE", "No valid input for timeout. Timeout was: '" . timeout "' . The script will now stop.")
			Measurement.Halt()
		}
		
		max := timeoutMsec 
		timerStop := A_TickCount + max
		this.image_Error := imageslist		
		res := Array()
		
		while (A_tickCount < timerStop and !this.imageNotFound) {
			Debug.logMessage("INFO", "Looping until image has disappeared.")
			res := this.yImageCheck(x1, y1, x2, y2, imageslist, timeout, False, options, True, customErrorMessage)
			error_code := res[5]
			index := res[1]
				if(error_code = 3) {
					element := res[6]
					; Match all characters that are not a "\\" starting from the end of the imagepath.
				    argument := "[^\\]+$"      
					RegExMatch(element, argument, result)
					Debug.logMessage("INFO", "Image: " . result . " could not be found anymore. Stopping the loop.")
					this.imageNotFound := true
					return index
				}
					
				if not(A_tickCount < timerStop) {					
					element := res[6]
					; Match all characters that are not a "\\" starting from the end of the imagepath.
					argument := "[^\\]+$"      
					RegExMatch(element, argument, result)
					Debug.logMessage("INFO", "Image: " . result . " did not disappear within timeout.")
					if (ThrowError) {
						if(customErrorMessage != "")
						{
							this.error(customErrorMessage) 
						} else {
							this.error("Image did not disappear within timeout")
						}
					}
					return index
					break
				} 
		} 
		
		this.imageNotFound := false  ; Clear the global imageFound boolean.
		this.image_Error := Object() ; Clear the global image_error object.
	} 
		
	; @doc Ymor Command - Custom mouseclick command which performs (a) mouseclick(s) on specified coordinates with specified mousebutton..
	; @param x/y, The x/y coordinates to which the mouse cursor is moved prior to clicking. Default = 0 (topleft screen)
	; @param button, Which mouse button to click. Default = left.
	; @param count, How many times to click. Default is 1. 
	yClick(x=0,y=0,button="left",count=1) {
		; You can use it but it is not recommended. 
		if (x < 0 or y < 0) {
			Debug.logMessage("INFO", "You are using negative coördinates in your yClick, please adjust them.")
		}
		; It is possible to click zero times in which case you just move the mouse to the coordinates specified. 
		if (count < 0 or count = 0) {
			Debug.logMessage("INFO", "You did not specify how many clicks should be executed, returning to default setting of one click.")
			count := 1
		}
		if not (button="left" or button="right" or button="middle") {
			Debug.logMessage("INFO", "You did not correctly specify which button to click, returning to default setting of left button.") 
		}
		; Click can't throw an error that is why we use the above warning messages and measurement should just continue. 
		Click,%x%,%y%,%button%,%count%
		Debug.logMessage("INFO", "Clicking " . count . " time(s) with " . button . " mouse button and coordinates x=" . x . ", y=" . y . ".")
		; Move mouse to coordinates 0,0 so mouse is not in the way with imagechecks.
		MouseMove, 0, 0
	}
	
	; @doc Ymor Command - yClickHold clicks on the specified coordinates and waits before releasing the button
	; @param x/y, The x and y coordinates where the mouse will do the click
	; @param button, Which mouse button to click. Default is left.
	; @param timeout, The timeout in msec before releasing the button
	yClickHold(x=0,y=0,button="left",timeout=500) {
		; You can use it but it is not recommended. 
		if (x < 0 or y < 0) {
			Debug.logMessage("INFO", "You are using negative coördinates in your yClick, please adjust them.")
		}
		if not (button="left" or button="right" or button="middle") {
			Debug.logMessage("INFO", "You did not correctly specify which button to click, returning to default setting of left button.") 
		}
		MouseMove,x, y
		Debug.logMessage("FINE", "Moving mouse at " . x . "x" . y) 
		Click, down, %button%
		Debug.logMessage("FINE", "Mouse button down: " . button) 
		this.msleep(timeout)
		Click, up, %button%
		Debug.logMessage("FINE", "Mouse button up: " . button) 
		; Move mouse to coordinates 0,0 so mouse is not in the way with imagechecks.
		MouseMove, 0, 0
	}
	
	 ; @doc Ymor Command - yImageClickHold same as yImageClick but would wait before releasing the mouse button
	 ; @param holdTimeout, number of seconds before releasing the mouse button
	yImageClickHold(x1, y1, x2, y2, clickOffsetX, clickOffsetY, imageslist, timeout="60sec", button="Left", holdTimeout=500, ThrowError=true, options="*20", customErrorMessage="") {
		r := this.yImageCheck(x1,y1,x2,y2,imageslist,timeout,ThrowError,options, customErrorMessage) 
		x := r[2] + clickOffsetX
		y := r[3] + clickOffsetY
		this.yClickHold(x,y,button,holdTimeout)
	}
	
	
	; @doc Ymor Command - yImageClick searches for an image and clicks on the image on the specified coordinates.
	; @param x1/y1, The X and Y coordinates of the upper left corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param x2/y2, The X and Y coordinates of the lower right corner of the rectangle to search, which can be expressions. Coordinates are relative to the active window unless CoordMode was used to change that.
	; @param clickOffsetX/Y, Offset in x and y coordinates so we correctly click on the image. 
	; @param imageslist, The list of images that will be checked for on the screen.
	; @param timeout, The timeout in msec, sec or mins. Default is 60 seconds.
	; @param button, Which mouse button to click. Default is left.
	; @param count, How many times to click. Default is 1.
	; @param ThrowError, Throw an error when the image can't be clicked. Default is true. 
	; @param options, Specify for options a number between 0 and 255 (inclusive) to indicate the allowed number of shades of variation in either direction for the intensity of the red, green, and blue components of each pixel's color. For example, *2 would allow two shades of variation. Default is 5. 
	; @param customErrorMessage, give a custom error message for errot tab in Ymonitor. If omitted a default message will be used.
	yImageClick(x1, y1, x2, y2, clickOffsetX, clickOffsetY, imageslist, timeout="60sec", button="Left", count=1, ThrowError=true, options="*5", customErrorMessage="") {
		
		this.returnValue := Object() 	; Clear the global returnValue array.
		result := Array()	 			; Clear the global result array.
		
		; Set all timeout modes to msec.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutMsec := match1
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match))  {
			timeoutMsec := match1 * 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match))  {
			timeoutMsec := match1 * 60000
		} 
		else 
		{
			Debug.logMessage("SEVERE", "No valid input for timeout. Timeout was: '" . timeout "' . The script will now stop.")
			Measurement.Halt()
		}		
		
		result := this.yImageCheck(x1,y1,x2,y2,imageslist,timeout,ThrowError,options,false,customErrorMessage)
		element := result[6]
		FoundX := result[2]
		FoundY := result[3]
		message := result[4]
		errorcode := result[5]
		index := result[1]
		
		; Strip the imagelocation, this is only for the logfile so it is better readable. We only want to log the image we search for and not the complete path.
		imagelocation := element
		argument := ".*\\"
		strippedimagelocation := RegExReplace(imagelocation, argument)
		
		if (errorcode = 1) {
			; If the image is found add the offset to the coordinates so we click correctly on the image.
			ClickX := FoundX + clickOffsetX
			ClickY := FoundY + clickOffsetY
			Debug.logMessage("INFO", "Image: " . strippedimagelocation . " was found. Clicking at x: " . ClickX " y: " . ClickY . " with OffsetX: " . clickOffsetX . " and OffsetY: " . clickOffsetY . "")
			Click,%button%,%ClickX%,%ClickY%,%count%
			
			if(this.enableScrenCapForDoc = true) {
				Sleep, 1000
				this.ySaveScreenshotDesigndocument(0, false, A_ScriptDir . "\yDesigndocument\" . this.name . "-" . strippedimagelocation . "_click.png")
			}
			return index
		}
		else if(errorcode = 2) {
			argument := "[^\\]+$"       
			RegExMatch(element, argument, result)
			Debug.logMessage("INFO", "Click was not performed. Image: " . result . " not found at specified coördinates, but was found at x: " . FoundX " y: " . FoundY . "")
			if (ThrowError)
			{
				if(customErrorMessage != "")
				{
					this.error(customErrorMessage)
					return index					
				} else {
					this.error("Could not click image: " . result . ", image not found at specified coördinates.")
					return index
				}
			}			
		}
		else if(errorcode = 3) {
			if (ThrowError)
			{
				if(customErrorMessage != "")
				{
					this.error(customErrorMessage)
					return index					
				} else {
					argument := "[^\\]+$"       
					RegExMatch(element, argument, result)		
					this.error("Could not click image: " . result . ", image was not found on screen.")
					return index
				}
			}	

			return index
			Debug.logMessage("INFO", "Click was not performed Image was not found on screen")
		}
	}
	
	; @doc Ymor Command - yPacing command is a custom pacing command used when creating loadtest scripts and sets a fixed timeout (sleep) between transactions.
	; @param Timeout for pacing. Default is 60 seconds. 
	yPacing(timeout="60sec") {
		; Set all timeout modes to msec.
		if (RegExMatch(timeout, "i)^(\d*)\w*msec\w*", match))  {
			timeoutMsec := match1
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*sec\w*", match))  {
			timeoutMsec := match1 * 1000
		} 
		else if (RegExMatch(timeout, "i)^(\d*)\w*min\w*", match))  {
			timeoutMsec := match1 * 60000
		} 
		else 
		{
			Debug.logMessage("SEVERE", "No valid input for timeout. Timeout was: '" . timeout "' . The script will now stop.")
			Mesurement.Halt()
		}
		Wacht := timeoutMsec - (totalscripttime * 1000)
		Debug.logMessage("INFO", "Pacing for " . Wacht . " milliseconds.")
		Sleep, Wacht
	}
	
	; @doc Ymor Command - ySplunkSend command will (at the end of each transaction) send the transaction results via TCP to defined splunk environment.
	; @doc for now below params are set in script not in function.
	; @param SendToSplunk, Is true, the script will send the results to splunk.
	; @param yValidateSaveScreenshot, is True, it will capture the screenshot on error of each transaction in the scriptdir/Yvalidate folder.
	; @param CustomSplunkString, if string is set, it will be added to splunk.
	; @param yTest, name of the test in the Ymor Yvalidate app.
	; @param yRun, name of the run in the Ymor Yvalidate app.
	; @param yScript, name of the script in the Ymor Yvalidate app.	
	ySplunkSend() {
		if(ySplunkSend = true) {
			try{
				FormatTime, datum, %A_Now%, yyyyMMdd
				FormatTime, tijd, %A_Now%, yyyyMMddHmm
				whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
				whr.setTimeouts(0,0,0,1)
				whr.Open("PUT",SplunkAdress, false)
				whr.SetRequestHeader("Content-Type", "application/json")		
				if (this.errorResult.getResultStatus() = 0) {
					if(yValidateSaveScreenshot = true) {
						yValidateScreenshotLocation := A_ScriptDir . "\Yvalidate"
						if !FileExist(yValidateScreenshotLocation) { 
							FileCreateDir, %yValidateScreenshotLocation%
						}
						yValidateScreenshotLocation := A_ScriptDir . "\Yvalidate\" . this.name . "_" . tijd . ".png"
						this.ySaveScreenshotYvalidate(0, false, yValidateScreenshotLocation)
					}
					whr.Send("" . CustomSplunkString . "ytest=" . yTest . ";agent=" . A_ComputerName . ";results=" . yRun . "_" . datum . ";script=" . yScript . ";userid=" . A_ComputerName . ";transname=" . this.transactionResult.getName() . ";timername=" . this.transactionResult.getName() . ";transtime=" . this.transactionResult.getDuration() . ";status=" . this.errorResult.getResultStatus() . ";nNodes=0;errormssg=" . this.errorResult.getMessage(message) . ";usergroup=0")
				} else {
					whr.Send("" . CustomSplunkString . "ytest=" . yTest . ";agent=" . A_ComputerName . ";results=" . yRun . "_" . datum . ";script=" . yScript . ";userid=" . A_ComputerName . ";transname=" . this.transactionResult.getName() . ";timername=" . this.transactionResult.getName() . ";transtime=" . this.transactionResult.getDuration() . ";status=" . this.errorResult.getResultStatus() . ";nNodes=0;usergroup=0")
				}	
			} catch {
				Debug.logMessage("SEVERE", "Something went wrong while sending the results to Splunk.")
			}
		Debug.logMessage("INFO", "Transaction result send to Splunk (" . SplunkAdress . ")")
		}
	}
	
	; @doc Ymor Command - yCreateDocumentation will create a screenshot on the start and stop of each transaction and save those screenshots in the &quot;%A_ScriptDir%\yDesigndocument\&quot; folder. Used to create the Ymor Designdocument for customers. 
	; @param yCreateDocumentation, boolean must be set to true in script to enable the creation of the screenshots, see template script via noobtool for usage.
	yCreateDocumentation() {
		if(yCreateDocumentation = true) {
			try{
					FormatTime, tijd, %A_Now%, yyyyMMddHmm
					if(yCreateDocumentation = true) {
						yDesigndocumentFolder := A_ScriptDir . "\yDesigndocument"
						if !FileExist(yDesigndocumentFolder) { 
							FileCreateDir, %yDesigndocumentFolder%
						}
						if(this.transactionStarting = true){
							yDesigndocumentLocation := A_ScriptDir . "\yDesigndocument\" . this.name . "_Start.png"
						}
						if(this.transactionStopping = true){
							yDesigndocumentLocation := A_ScriptDir . "\yDesigndocument\" . this.name . "_Stop.png"
						}
						this.ySaveScreenshotDesigndocument(0, false, yDesigndocumentLocation)
					}
			}
			catch {
				Debug.logMessage("SEVERE", "Something went wrong while taking screenshots of a transaction start/stop to the %A_ScriptDir%\yDesigndocument\ folder. Please check if you have rights to create that folder and/or re-run the script.")
			}
		}
	}

	; @doc Ymor Command - yKillAllExcept kills all open programs except the ones that are added to the exclusionlist by classname or Wintitle name. Useful at startup/end of a script, ensures a clean run each time.
	; @param ClassList, list with Class names to add to the Exclusion List. Use (Active Window Info) in Scite to find Class names or set TestRun to true.
	; @param WinTitleList, list with WinTitle names to add to the Exclusion List. Use (Active Window Info) in Scite to find Wintitle names or set TestRun to true. Only full wintitles are allowed in this function even if SetTitleMatchMode is set to 2.
	; @param TestRun, boolean, if set to true it will show message boxes of all open programs and what will happen to each program (kill or no kill). 
	yKillAllExcept(ClassList,WinTitleList,TestRun=true) {
		try{
			yClassList := Object() 		; Clear the global ClassList.
			yWinTitleList := Object() 	; Clear the global WinTitleList.
			yClassList := ClassList ; fill yClassList with ClassList.
			yWinTitleList := WinTitleList ; fill yWinTitleList with WinTitleList.
			
			; create and append the "haystack" ExclusionList with all defined classes and Wintitles.
			For index, foundclass in yClassList {
				ExclusionList := ExclusionList .  " " . foundclass . ""
			}
			For index, foundWinTitle in yWinTitleList {
				ExclusionList := ExclusionList .  " " . foundWinTitle . ""
			}
			; add windows kill screen to exclusion list
			ExclusionList:= ExclusionList . " Shell_TrayWnd Button"
			if(TestRun=true){ 
				Loopcount := 1 
			} else { 
				Loopcount := 2
			}
			loop, %Loopcount% { ; loop two times for popups
				
				WinGet, id, list,,, Program Manager ; Command that gets all open programs except Windows Program Manager
				Loop, %id% ; Loop through all open programs
				{
					; get class and title of all ID's/.
					this_id := id%A_Index%
					WinGetClass, this_class, ahk_id %this_id% 
					WinGetTitle, this_title, ahk_id %this_id%

					; programs that have to stay open.
						If InStr(ExclusionList, this_class, false) AND (this_class!="") OR InStr(ExclusionList, this_title, false) AND (this_title!="") {
							if(TestRun=true){
								MsgBox, %this_class% or %this_title% was found in Classlist or WinTitleList (%FinalString%)  or was added to exclusionList and will therefore NOT be killed.
							}
						} else {
							if(TestRun=true){
								MsgBox, %this_class% or %this_title% was not found in Classlist or WinTitleList (%ExclusionList%) and will therefore be killed if TestRun is set to False.
							} else {
								WinKill, ahk_id %this_id% ; Killshot
							}
						}
				}
			}
		} catch {
			Debug.logMessage("SEVERE", "Something went wrong while killing the open programs. Advice is to run function in Testrun mode (set last parameter to true) to see what is causing the issue.")
		}
	}
}