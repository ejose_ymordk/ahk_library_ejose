﻿#Include ymorlib\Debug.ahk ; Include the lib files this class needs.  
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

/**
 * Class that is used to for specific file functions for use in scripts. 
 * 
 */

class FileFunctions { 

	; @doc read sentinel dependent parameters from a file.
	; @doc return an associative array with values indexed by param name.
	; @doc if key not found, return an empty array.
	; @param filename relative to script dir, comma seperated, first column has computername, firstline is headerline.
	; @param key normally a_computername
	ReadCSV(filename, key) {
			 
		file := FileOpen(A_Scriptdir . "\" . filename, "r `n")
		this.params := Object()
			
		if !IsObject(file)
		{
			Debug.logMessage("SEVERE", "Can't open the file:" . file " in the script directory.")
		} 
		else 
		{
			header := file.readline()
			; Remove all empty lines.
			StringReplace, header2, header, `n, 
			; Split an input in array
			StringSplit, header_array, header2, `,
			; Read the whole file. 
			while (not file.ateof) {
				line := file.readline()
				StringReplace, line2, line, `n, 
				StringSplit, line_array, line2, `, , `n
				; Equal (=), case-sensitive-equal (==)!
				if (key = line_array1) {
					Loop, %header_array0%
					{
						this.params[header_array%a_index%] := line_array%a_index%
					}
				}
			}	
		}
		return this.params	
	}

	; @doc Clear the files in the output2 folder when running a new measurement. 
	; @doc Use when you want to create clean logfiles with each script run. 
	clearLogFolder() {
		this.fileLocation := A_ScriptDir . "\output2\*.txt"
		FileDelete, % this.fileLocation
		this.fileLocation := A_ScriptDir . "\output2\*.png"
		FileDelete, % this.fileLocation
		Debug.LogMessage("INFO", "Logfiles have been cleared in the output2 directory. Continuing with clean slate.")
	}
	
	ClearDesignDocumentFolder() {
		this.fileLocation := A_ScriptDir . "\yDesigndocument\*.*"
		FileDelete, % this.fileLocation
		Debug.LogMessage("INFO", "Logfiles have been cleared in the yDesigndocument directory. Continuing with clean slate.")
	}

}


	