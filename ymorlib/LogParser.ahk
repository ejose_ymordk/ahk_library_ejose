﻿#include ymorlib\Debug.ahk ; Include the lib files this class needs.  
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

/**
 * Class that is used to parse the results of the script run to a logfile. 
 * This class is used for backwards compatibility with the Ymonitor parser. 
 * 
 * Two logfiles are created. One logfile which is send to the Ymonitor API, called logfile.txt, is also used for local analysis. 
 * The other logfile that is being created, called results.txt, is a stripped version with only transaction result information. 
 * Results.txt is currently not being send to Ymonitor API. Future would be nice to send only results to the API. This file can also be used in combination with Splunk since it only contains the results. 
 */
global totalscripttime

class LogParser {
	
	; @doc Create a new ScriptRun object
	; @param scriptResult, result of the script
	; @param fileLocation, location of the logfile
	__New(scriptResult, fileLocation) {
		this.scriptResult := scriptResult
		this.fileLocation := A_ScriptDir . "\output2"
		
		if (!fileLocation) {
			; Use default file location which is "%scriptdir%/output2". We already set this when we create the static object in class Transaction.ahk. 
		}
		else {
			this.fileLocation := fileLocation 
			; If the directory doesn't exist, create it. 
			if !FileExist(fileLocation) { 
				FileCreateDir, %fileLocation% 
				; If we can't create the directory try to create the logfile in the default directory.
				; To-Do -> User does not see when it goes wrong, we need to document or trigger this so user knows that if logfile is not created at specified location it will try the default location.
				if (ErrorLevel) {
					fileLocation := A_ScriptDir . "\output2" 
					if !FileExist(fileLocation) { 
						FileCreateDir, %fileLocation% 
					}
					this.fileName := fileLocation . "\results.txt"
					Debug.logMessage("SEVERE", "Could not create logfile at:" . fileLocation . ". Created logfile in output2 map in scriptdir") 
				}
			}
			this.fileName := fileLocation . "\results.txt"
		}
	}


	; @doc Parse function which parses the scriptrun results to results.txt
	parse() {
		Debug.logMessage("FINE", "-------------------------------------------------------------------- RESULTS ------------------------------------------------------------------------------------")
		Debug.logMessage("FINE", "Start Parsing Result")
		
		; Get the transaction results from the script run. 
		this.transactionResults := this.scriptResult.getTransactionResults() 
		; Open the logfile for appending the results.
		this.file := FileOpen(this.fileName, "w `n") 		
		Debug.logMessage("FINE", "File Opened: " . this.fileName)
		
		for index, transactionResults in this.transactionResults {
			for index, transactionResult in transactionResults {
				Debug.logMessage("FINE", "Transactionresult found: " . transactionResult.getName())
				result := transactionResult.getErrorResult()
				FormatTime, time, %A_Now%, yyyy-MM-dd HH:mm:s
				this.file.WriteLine("[" time "." A_Msec "] [RESULT] Transaction name: " . transactionResult.getName() . ", Status: " . result.getResultStatus() . ", Transaction time: "  . transactionResult.getDuration()) 
				Debug.logMessage("INFO", "Transaction finished: " . transactionResult.getName() . ", Success: " . result.getResultStatus() . ", Transaction time (sec): " transactionResult.getDuration() . "")
			}
		}															
		Debug.logMessage("INFO", "Total script time in seconds: " . totalscripttime )
		this.file.Close()
		Debug.logMessage("FINE", "-------------------------------------------------------------------- END RESULTS ---------------------------------------------------------------------------------")
	} 	
} 