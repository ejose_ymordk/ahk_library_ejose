﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

class FileMetaDataResult {
	
	; declare variable
	creationDate := ""
	name := ""
	duration := 0
	startDate := A_TickCount 
	endDate := A_TickCount
	; result can be 0 for failed or 1 for succes
	resultStatus := ""
	metaDataResults := ""
	errorResults := "" 
	message := ""
	
	; build constructor
	__New(name) {
    this.name := name
	}
		
	; the main function to set the resultStatus
	setResultStatus(resultStatus){
		this.resultStatus := resultStatus
	}
	
	getResultStatus(resultStatus){
		return this.resultStatus
	}
	
	setMessage(message){
		this.message := message
	}
	
	getMessage(message){
		return this.message
	}
	
	setCreationDate(creationDate) { 
		this.creationDate := creationDate
	}
	
	getCreationDate(creationDate) {
		return this.creationDate
	}
	
	setName(name) {
		this.name := name
	}
	
	getName(name) {
		return this.Name
	}
	
	setDuration(duration) {
		this.duration := duration 
	}
	
	getDuration(duration) { 
		return this.duration
	}
	
	setStartDate(startDate) {
		this.startDate := startDate 
	}
	
	getStartDate(startDate) {
		return this.startDate
	}
	
	setEndDate(endDate) {
		this.endDate := endDate
	}
	
	getEndDate(endDate) {
		return this.endDate
	}
	
	setMetaDataResults(metaDataResults) { 
		this.metaDataResults := metaDataResults
	}
	
	getMetaDataResults(metaDataResults) {
		return this.metaDataResults
	}
	
	setErrorResults(errorResults) {
		this.errorResults := errorResults
	}
	
	getErrorResults(errorResults) {
		return this.errorResults
	}
	
}