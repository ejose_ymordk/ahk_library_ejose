﻿
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


class Notification { 
	static Key := Notification.setup()
	
	setup() {
		; Create global debug object.
		global Notification := new Notification()
		; Create a filelocation, default filelocation is used.
		this.toolTipX := (A_ScreenWidth/2)
		this.toolTipY := 0
		CoordMode, ToolTip 
	}
	__New() {
		
	}	
	
	show(msg) {
		x := this.toolTipX
		y := this.toolTipY
		ToolTip,%msg%,x,y
	}
	
	showXY(msg,x,y) {
		ToolTip,%msg%,x,y
	}
	
	setpos(x,y) {
		this.toolTipX := x
		this.toolTipY := y
	}
	
	hide() {
		ToolTip
	}
}