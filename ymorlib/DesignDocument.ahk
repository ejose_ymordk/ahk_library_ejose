; Include the lib files this class needs.  
#include ymorlib\Measurement.ahk
#Include ymorlib\ZipFile.ahk

#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

/**
* Main class that controls the measurement runs. 
*
*/

class DesignDocument {

	yCreateDesignDocument(ServiceName="CubeViewName", CustomerName="NameOfCustomer", CommissioningParty="NameOfCommissioningParty", YmorSDM="NameOfYmorSDM", AuthorName="NameOfAuthor") {
		; Create Variables
		FormatTime, LongDate,, LongDate
		FormatTime, YMDTimeStamp,, yyyy_MM_dd
		yDesigndocumentImageDir := A_ScriptDir . "\yDesigndocument"
		ImageCount := 0
		Loop, %yDesigndocumentImageDir%\*.png {
			ImageCount := ImageCount+1
			MsgBox, ImageCount = %ImageCount%
		}	
		transactionCount := Round(ImageCount/2)
		MsgBox,  ImageCount = %ImageCount% transactionCount = %transactionCount%
		TemplateFolder := A_ScriptDir . "\ymorlib\designDocumentTemplates_2019\Template" . transactionCount
		
		TemplateFolderMediaPNGFiles := TemplateFolder . "\word\media\*.png"
		TemplateFolderMedia := TemplateFolder . "\word\media"
		ImageStartCount := 4
		TempFiles := A_ScriptDir . "\ymorlib\designDocumentTemplates_2019\TempFiles"
		MsgBox, TemplateFolder = %TemplateFolder%
		MsgBox, TempFiles = %TempFiles%
		RegExMatch(A_ScriptName, "\w\w\w\w++", ScriptName)
		DocumentName := "DesignDocument_" . ScriptName . "_" . YMDTimeStamp
		DesignDocumentZipName := A_ScriptDir . "\yDesigndocument\DesignDocument_" . ScriptName . "_" . YMDTimeStamp . ".zip"
		DesignDocumentDocxName := A_ScriptDir . "\yDesigndocument\DesignDocument_" . ScriptName . "_" . YMDTimeStamp . ".docx"
		
		;~ Check for common errors
		yDesigndocumentImageDir := A_ScriptDir . "\yDesigndocument"
		if !FileExist(yDesigndocumentImageDir) { 	
			Debug.logMessage("ERROR", "Trying to create Designdocumentation while no screenshots are taken. Run (ySaveScreenshots) commands first.") 
			throw Exception("ERROR: Trying to create Designdocumentation while no screenshots are taken. Run (ySaveScreenshots) commands first.", -1)
		}
		if (transactionCount > 15) {
			Debug.logMessage("ERROR", "Unsuported number of Ymonitortransactions, max is 15. Recommend to create a template which support more transactions, or disable transactions in script to fit the max amount of 15.") 
			throw Exception("ERROR: Unsuported number of Ymonitor transactions, max is 15. Recommend to create a template which support more transactions, or disable transactions in script to fit the max amount of 15.", -1)		
		}
		if (yCreateDocumentation = false) {
			Debug.logMessage("ERROR", "yCreateDocumentation is set to false so there are no transaction screenshots to create designdocument. Set to true and rerun the script.") 
			throw Exception("ERROR: yCreateDocumentation is set to false so there are no transaction screenshots to create designdocument. Set to true and rerun the script.", -1)
		}


		; All Textual Edits
			;~ Copy document to temporary location, this for editting and restoring after document creation.
			FileCopy, %TemplateFolder%\word\document.xml, %TempFiles%\document.xml, true
			;~ Read the document.xml for editting purposes.
			FileRead, line, %TemplateFolder%\word\document.xml
			;~ Replace strings with edits.
			StringReplace, line, line, yServiceName, %ServiceName%, All
			StringReplace, line, line, yCustomerName, %CustomerName%, All
			StringReplace, line, line, yCommissioningParty, %CommissioningParty%, All
			StringReplace, line, line, yYmorSDM, %YmorSDM%, All
			StringReplace, line, line, yVersionDate, %LongDate%, All
			StringReplace, line, line, yDate, %LongDate%, All
			StringReplace, line, line, yAuthorName, %AuthorName%, All
			StringReplace, line, line, yDocumentName, %DocumentName%, All
			
			;~ Extract transaction names and store in vars
			TNameCnt := 0
			LCnt := 0
			alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ

			; Retrieve Images sorted by aplhabetic order (1,2,3,4)
			FileList =  ; Initialize to be blank.
			Loop, %yDesigndocumentImageDir%\*.png 
			FileList = %FileList%%A_LoopFileName%`n
			Sort, FileList  ; The R option sorts in reverse order. See Sort for other options.
			Loop, parse, FileList, `n
			{
				LCnt++
				if (LCnt = 1) or (LCnt = 3) or (LCnt = 5) or (LCnt = 7) or (LCnt = 9) or (LCnt = 11) or (LCnt = 13) or (LCnt = 15) or (LCnt = 17) or (LCnt = 19) or (LCnt = 21) or (LCnt = 23) or (LCnt = 25) or (LCnt = 27) or (LCnt = 29){
					TNameCnt++
					;~ trim transaction name from image.. and store in variabele transname.
					RegExMatch(A_LoopField, "[^\d_ ](?:\w[^_ ]*+(?=_))*+(?=_)", transname)
					transname := StrReplace(transname, "_", A_Space)
					;~ set string to Title Case Like This.
					StringLower, transname, transname, T
					;~ Set transaction name based on aplhabet
					AlphabetCount := SubStr(alphabet, TNameCnt,1)
					AlphabetCount := "yTransactionName" . AlphabetCount
					StringReplace, line, line, %AlphabetCount%, %transname%, All
				}	
			}

			FileDelete, %TemplateFolder%\word\document.xml
			FileAppend, %line%, %TemplateFolder%\word\document.xml
		;~ All Image Edits
			;~ add files to temp, restore at end of function
			FileCopy, %TemplateFolderMediaPNGFiles%, %TempFiles%, true
			;~ Loop through image dir and copy to template dir, starting with image 3 which is set by ImageStartCount
			Loop Files, %yDesigndocumentImageDir%\*.png
			{
				;~ MsgBox, Files = %A_LoopFileLongPath%
				TemplateFolderMediaWithCount := TemplateFolderMedia . "\image" . ImageStartCount . ".png"
				ImageStartCount++
				FileCopy, %A_LoopFileLongPath%, %TemplateFolderMediaWithCount%, true
			}

		; Create ZIP file
			Zip := new ZipFile(DesignDocumentZipName)
			;~ Add files to Zip File
			SetWorkingDir, %TemplateFolder%
			Zip.Pack()
			SetWorkingDir %A_ScriptDir%

			;~ Change ZipFile To Docx
			FileCopy, %DesignDocumentZipName%, %DesignDocumentDocxName%
			FileDelete, %DesignDocumentZipName%

		;~ Cleanup and Restore Temp
			;~ Remove Text edits for new run
			FileCopy, %TempFiles%\document.xml, %TemplateFolder%\word\document.xml, true
			FileDelete, %TempFiles%\document.xml
			;~ Remove Image edits for new run
			FileCopy, %TempFiles%, %TemplateFolder% . "\word\media\", true
			FileDelete, %TempFiles%\*.png
			FileDelete, %DesignDocumentZipName%
	}
}